"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const store_1 = require("./store");
const logger_1 = require("./logger");
(0, logger_1.startLogging)();
setInterval(() => {
    const game = {
        id: Math.random().toString(),
        moves: []
    };
    store_1.localStorage.addGame(game);
}, 5000);
