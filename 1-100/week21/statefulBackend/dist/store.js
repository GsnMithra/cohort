"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.localStorage = exports.LocalStorage = void 0;
class LocalStorage {
    constructor() {
        this.games = [];
    }
    getGames() {
        return this.games;
    }
    addGame(game) {
        this.games.push(game);
    }
}
exports.LocalStorage = LocalStorage;
// singleton pattern
exports.localStorage = new LocalStorage();
