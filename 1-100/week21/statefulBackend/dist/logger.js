"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.startLogging = void 0;
const store_1 = require("./store");
const startLogging = () => {
    setInterval(() => {
        console.log(store_1.localStorage.getGames());
    }, 5000);
};
exports.startLogging = startLogging;
