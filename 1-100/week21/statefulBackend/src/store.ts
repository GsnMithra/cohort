export type Game = {
    id: string;
    moves: string[];
}

class LocalStorage {
    games: Game[];

    private constructor() {
        this.games = []
    }

    private static instance: LocalStorage
    static getInstance() {
        if (!LocalStorage.instance)
            LocalStorage.instance = new LocalStorage();
        return LocalStorage.instance
    }

    getGames() {
        return this.games;
    }

    addGame(game: Game) {
        this.games.push(game);
    }
}

// okaish approach
// export const localStorage = new LocalStorage();

// singleton approach
export const localStorage = LocalStorage.getInstance();
