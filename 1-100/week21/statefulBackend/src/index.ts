import { Game, localStorage } from "./store";
import { startLogging } from "./logger";

startLogging();

setInterval(() => {
    const game: Game = {
        id: Math.random().toString(),
        moves: []
    }
    localStorage.addGame(game)
}, 5000)
