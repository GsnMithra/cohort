import { createClient } from 'redis';

const publishExchangePrices = async () => {
    const client = createClient();
    await client.connect()

    setInterval(async () => {
        await client.publish("AAPL", JSON.stringify({ price: Math.random() * 100 }));
        await client.publish("META", JSON.stringify({ price: Math.random() * 100 }));
        await client.publish("ALPH", JSON.stringify({ price: Math.random() * 100 }));
    }, 100);
}

publishExchangePrices();
