"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.pubSubManager = void 0;
const redis_1 = require("redis");
const _1 = require(".");
class PubSubManager {
    constructor() {
        this.userStockMapping = new Map();
        this.redisClient = (0, redis_1.createClient)();
        this.redisClient.connect();
        setInterval(() => {
            console.log(this.userStockMapping);
        }, 1000);
    }
    static getInstance() {
        if (!PubSubManager.instance)
            PubSubManager.instance = new PubSubManager();
        return PubSubManager.instance;
    }
    subscribe(userId, stockTicker) {
        var _a;
        if (!this.userStockMapping.has(stockTicker))
            this.userStockMapping.set(stockTicker, []);
        (_a = this.userStockMapping.get(stockTicker)) === null || _a === void 0 ? void 0 : _a.push(userId);
        this.redisClient.subscribe(stockTicker, (message) => {
            const price = JSON.parse(message);
            this.forwardPriceToUser(stockTicker, userId, price);
        });
    }
    unsubscribe(userId, stockTicker) {
        return __awaiter(this, void 0, void 0, function* () {
            var _a, _b;
            this.userStockMapping.set(stockTicker, ((_a = this.userStockMapping.get(stockTicker)) === null || _a === void 0 ? void 0 : _a.filter((id) => id !== userId)) || []);
            if (((_b = this.userStockMapping.get(stockTicker)) === null || _b === void 0 ? void 0 : _b.length) === 0)
                this.redisClient.unsubscribe(stockTicker);
        });
    }
    unsubscribeAll(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            this.userStockMapping.forEach((users, stockTicker) => __awaiter(this, void 0, void 0, function* () {
                if (users.includes(userId)) {
                    yield this.unsubscribe(userId, stockTicker);
                }
            }));
        });
    }
    forwardPriceToUser(stockTicker, userId, price) {
        var _a, _b;
        if (!_1.clients.has(userId))
            return;
        if (!((_a = this.userStockMapping.get(stockTicker)) === null || _a === void 0 ? void 0 : _a.includes(userId)))
            return;
        (_b = _1.clients.get(userId)) === null || _b === void 0 ? void 0 : _b.send(JSON.stringify({
            stockTicker,
            price
        }));
    }
}
exports.pubSubManager = PubSubManager.getInstance();
