"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.clients = void 0;
const PubSubManager_1 = require("./PubSubManager");
const http_1 = __importDefault(require("http"));
const ws_1 = require("ws");
exports.clients = new Map();
const server = http_1.default.createServer((req, res) => {
    res.end('hello!');
});
const wss = new ws_1.WebSocketServer({ server });
wss.on('connection', (socket) => {
    socket.userId = Math.random().toString(36).slice(2);
    exports.clients.set(socket.userId, socket);
    socket.on('message', (message) => {
        const { stockTicker, action } = JSON.parse(message);
        if (action === 'subscribe') {
            PubSubManager_1.pubSubManager.subscribe(socket.userId, stockTicker);
        }
        else if (action === 'unsubscribe') {
            PubSubManager_1.pubSubManager.unsubscribe(socket.userId, stockTicker);
        }
    });
    socket.on('close', () => {
        exports.clients.delete(socket.userId);
        PubSubManager_1.pubSubManager.unsubscribeAll(socket.userId);
    });
    socket.send(JSON.stringify({
        message: 'connection established'
    }));
});
const PORT = 8080;
server.listen(PORT, () => {
    console.log(`Server running on ${PORT}`);
});
