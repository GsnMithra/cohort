import { RedisClientType, createClient } from "redis"
import { clients } from "."

class PubSubManager {
    private static instance: PubSubManager
    private userStockMapping: Map<string, string[]>
    private redisClient: RedisClientType
    private constructor() {
        this.userStockMapping = new Map<string, string[]>()
        this.redisClient = createClient()
        this.redisClient.connect()
    }

    public static getInstance() {
        if (!PubSubManager.instance)
            PubSubManager.instance = new PubSubManager()
        return PubSubManager.instance
    }

    public subscribe(userId: string, stockTicker: string) {
        if (!this.userStockMapping.has(stockTicker))
            this.userStockMapping.set(stockTicker, [])
        this.userStockMapping.get(stockTicker)?.push(userId)

        this.redisClient.subscribe(stockTicker, (message) => {
            const price = JSON.parse(message)
            this.forwardPriceToUser(stockTicker, userId, price)
        })
    }

    public async unsubscribe(userId: string, stockTicker: string) {
        this.userStockMapping.set(stockTicker, this.userStockMapping.get(stockTicker)?.filter((id) => id !== userId) || [])
        if (this.userStockMapping.get(stockTicker)?.length === 0)
            this.redisClient.unsubscribe(stockTicker)
    }

    public async unsubscribeAll(userId: string) {
        this.userStockMapping.forEach(async (users, stockTicker) => {
            if (users.includes(userId)) {
                await this.unsubscribe(userId, stockTicker)
            }
        })
    }

    public forwardPriceToUser(stockTicker: string, userId: string, price: string) {
        if (!clients.has(userId))
            return
        if (!this.userStockMapping.get(stockTicker)?.includes(userId))
            return

        clients.get(userId)?.send(JSON.stringify({
            stockTicker,
            price
        }))
    }
}

export const pubSubManager = PubSubManager.getInstance()
