import { pubSubManager } from "./PubSubManager";
import http from "http"
import { WebSocketServer, WebSocket } from "ws";

export const clients: Map<string, WebSocket> = new Map()
const server = http.createServer(
    (req, res) => {
        res.end('hello!');
    }
)

declare module 'ws' {
    interface WebSocket {
        userId: string
    }
}

const wss = new WebSocketServer({ server })

wss.on('connection', (socket: WebSocket) => {
    socket.userId = Math.random().toString(36).slice(2)
    clients.set(socket.userId, socket)

    socket.on('message', (message: string) => {
        const { stockTicker, action } = JSON.parse(message)
        if (action === 'subscribe') {
            pubSubManager.subscribe(socket.userId, stockTicker)
        } else if (action === 'unsubscribe') {
            pubSubManager.unsubscribe(socket.userId, stockTicker)
        }
    })

    socket.on('close', () => {
        clients.delete(socket.userId)
        pubSubManager.unsubscribeAll(socket.userId)
    })

    socket.send(JSON.stringify({
        message: 'connection established'
    }))
})

const PORT = 8080;
server.listen(PORT, () => {
    console.log(`Server running on ${PORT}`)
})

