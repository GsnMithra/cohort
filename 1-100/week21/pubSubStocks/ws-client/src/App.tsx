import { useEffect, useState } from 'react'
import './App.css'

function App() {
    const [socket, setSocket] = useState<WebSocket | null>(null)
    const [currentSubscriptions, setCurrentSubscriptions] = useState<string[]>([])
    const [stockPrices, setStockPrices] = useState<{
        AAPL: number;
        META: number;
        ALPH: number;
    }>({
        AAPL: 0,
        META: 0,
        ALPH: 0,
    })

    useEffect(() => {
        const socket = new WebSocket('ws://localhost:8080')

        socket.onopen = () => {
            setSocket(socket)
        }

        socket.onmessage = (event) => {
            const { stockTicker, price, message } = JSON.parse(event.data)
            if (message)
                return

            setStockPrices((prev) => ({
                ...prev,
                [stockTicker]: price.price
            }))
        }

        return () => {
            socket.close()
        }
    }, [])

    const handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (!socket)
            return

        const { id, checked } = event.target
        if (checked) {
            socket.send(JSON.stringify({
                stockTicker: id,
                action: 'subscribe'
            }))
            setCurrentSubscriptions((prev) => [...prev, id])
        }
        else {
            socket.send(JSON.stringify({
                stockTicker: id,
                action: 'unsubscribe'
            }))
            setCurrentSubscriptions((prev) => prev.filter((stock) => stock !== id))
        }
    }

    if (!socket)
        return <h1>Connecting to the server...</h1>

    return (
        <>
            <h1>Stock price logger</h1>
            <div id="stock-prices">
                <input type="checkbox" id="AAPL" onChange={handleCheckboxChange} />
                <label htmlFor="AAPL">AAPL</label>
                <input type="checkbox" id="META" onChange={handleCheckboxChange} />
                <label htmlFor="META">META</label>
                <input type="checkbox" id="ALPH" onChange={handleCheckboxChange} />
                <label htmlFor="ALPH">ALPH</label>
            </div>
            <div id="stock-prices">
                {currentSubscriptions.map((stock, id) => (
                    // @ts-ignore
                    <p key={id}>{stock}: {stockPrices[stock]}</p>
                ))}
            </div>
        </>
    )
}

export default App
