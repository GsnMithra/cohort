"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const yaml_1 = __importDefault(require("yaml"));
const swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
const express_1 = __importDefault(require("express"));
const openApiSpec = yaml_1.default.parse(fs_1.default.readFileSync("./src/openapispec.yaml", "utf-8"));
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.post("/hello", (req, res) => {
    const token = req.headers.authorization;
    const { name } = req.body;
    const { age, roll } = req.query;
    res.json({
        message: `Hello ${name}!`,
        age,
        roll,
        token
    });
});
app.use('/docs', swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(openApiSpec));
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
