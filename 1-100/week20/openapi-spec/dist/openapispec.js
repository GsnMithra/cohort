"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.openApiSpec = void 0;
exports.openApiSpec = {
    "openapi": "3.1.0",
    "info": {
        "title": "Mithra API",
        "description": "API for Mithra",
        "version": "1.0.0"
    },
    "servers": [
        {
            "url": "http://localhost:8080"
        }
    ],
    "paths": {
        "/hello": {
            "post": {
                "summary": "Hello message",
                "description": "Returns a hello message to the user",
                "requestBody": {
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "object",
                                "properties": {
                                    "name": {
                                        "type": "string",
                                        "description": "Name of the user"
                                    }
                                }
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Hello message",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "description": "Hello message to the user"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
};
