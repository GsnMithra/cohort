import fs from "fs"
import yaml from "yaml"
import swaggerUi from "swagger-ui-express"
import express from "express"

const openApiSpec = yaml.parse(fs.readFileSync("./src/openapispec.yaml", "utf-8"))
const app = express()
app.use(express.json())

app.post("/hello", (req, res) => {
    const token = req.headers.authorization
    const { name } = req.body
    const { age, roll } = req.query

    res.json({
        message: `Hello ${name}!`,
        age,
        roll,
        token
    })
})

app.use('/docs', swaggerUi.serve, swaggerUi.setup(openApiSpec))

const PORT = process.env.PORT || 8080
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`)
})
