"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class DefaultService {
    /**
     * Hello message
     * Returns a hello message to the user
     * @param age age of the user
     * @param roll roll of the user
     * @param requestBody
     * @returns any Hello message
     * @throws ApiError
     */
    static postHello(age, roll, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/hello',
            query: {
                'age': age,
                'roll': roll,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }
}
exports.DefaultService = DefaultService;
