/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class DefaultService {
    /**
     * Hello message
     * Returns a hello message to the user
     * @param age age of the user
     * @param roll roll of the user
     * @param requestBody
     * @returns any Hello message
     * @throws ApiError
     */
    public static postHello(
        age: string,
        roll: string,
        requestBody: {
            /**
             * Name of the user
             */
            name?: string;
        },
    ): CancelablePromise<{
        /**
         * Hello message
         */
        message?: string;
        /**
         * age of the user
         */
        age?: string;
        /**
         * roll of the user
         */
        roll?: string;
        /**
         * JWT token
         */
        token?: string;
    }> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/hello',
            query: {
                'age': age,
                'roll': roll,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }
}
