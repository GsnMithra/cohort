import { useState } from "react";
import "./App.css";

function App() {
    const [todoList, setTodoList] = useState([]);
    const [todo, setTodo] = useState("");

    function addTodoItem() {
        if (todo === "") return;

        setTodoList((prevTodoList) => [...prevTodoList, todo]);
        setTodo("");
    }

    function changeTodoText(e) {
        setTodo(e.target.value);
    }

    return (
        <div>
            <div id="todo-operator">
                <input
                    type="text"
                    value={todo}
                    onChange={changeTodoText}
                ></input>
                <button onClick={addTodoItem}>Add todo</button>
            </div>

            <ul id="todo-list">
                {todoList.map((item, index) => (
                    <li key={index}>{item}</li>
                ))}
            </ul>
        </div>
    );
}

export default App;
