// wrapper component for styles
const Card = ({ children }) => {
    const cardStyles = {
        border: "1px solid black",
        borderRadius: "1rem",
        padding: "5px",
        margin: "10px",
    };

    return <div style={cardStyles}>{children}</div>;
};

export default Card;
