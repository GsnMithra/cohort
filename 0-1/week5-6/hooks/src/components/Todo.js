import { useState, useEffect } from "react";
import axios from "axios";

const Todo = ({ id = undefined, title, description }) => {
    const [todo, setTodo] = useState({
        title: title ?? "",
        description: description ?? "",
    });

    useEffect(() => {
        const uri = "https://sum-server.100xdevs.com/todo";
        if (id !== undefined)
            axios.get(`${uri}?id=${id}`).then((res) => setTodo(res.data.todo));
    }, [id]);

    return (
        <div>
            <h2>{todo.title}</h2>
            <h4>{todo.description}</h4>
        </div>
    );
};

export default Todo;
