import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Effect from "./hookComponents/Effect";
import Memoize from "./hookComponents/Memoize";
import Callback from "./hookComponents/Callback";
import Custom from "./hookComponents/Custom";
import { useState } from "react";

const HomePage = () => {
    const [count, setCount] = useState(0);

    return (
        <>
            <button
                onClick={() => {
                    // good way to update state
                    setCount((a) => a + 1);
                    setCount((a) => a + 1);

                    // bad way to update state
                    // setCount(count + 1);
                    // setCount(count + 1);
                }}
            >
                Count: {count}
            </button>
        </>
    );
};

function App() {
    return (
        <Router>
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/callback" element={<Callback />} />
                <Route path="/effect" element={<Effect />} />
                <Route path="/memo" element={<Memoize />} />
                <Route path="/custom" element={<Custom />} />
            </Routes>
        </Router>
    );
}

export default App;
