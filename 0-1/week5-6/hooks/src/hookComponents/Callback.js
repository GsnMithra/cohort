import { memo, useCallback, useState } from "react";

const Component = memo(({ a }) => {
    a();
    return <h1>Hello</h1>;
});

export default function Callback() {
    const [count, setCount] = useState(0);
    const [myCount, setMyCount] = useState(0);

    // when a function gets passed as a prop to a component, it gets re-created on every re-render
    // function a () == function b () -> false
    // function a() {
    //     console.log("hi");
    // }

    // we are checking the reference of the function, not the function itself
    // if reference is same, then react will not re-render the component
    const loggerFunction = useCallback(
        function () {
            // myCount not included in the dependency array
            // so it will not re-create the function on every re-render
            console.log(`the count is ${count} and myCount is ${myCount}`);
        },
        [count],
    );

    // * when props change, the component re-renders, when function gets defined inside the component,
    // it gets re-created on every re-render, leading to unnecessary re-renders of the child component where function is passed to,
    // so we use useCallback to memoize the function and prevent unnecessary re-renders, and only create the function when the dependency changes

    const componentStyles = {
        padding: "10px",
        margin: "10px",
    };

    return (
        <>
            <button onClick={() => setCount(count + 1)}>Count: {count}</button>
            <button onClick={() => setMyCount(myCount + 1)}>
                MyCount: {myCount}
            </button>
            <div style={componentStyles}>
                <Component a={loggerFunction} />
            </div>
        </>
    );
}
