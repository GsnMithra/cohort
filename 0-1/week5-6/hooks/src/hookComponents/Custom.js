import { useState, useEffect } from "react";
import axios from "axios";
import Todo from "../components/Todo";
import Card from "../components/Card";

// name of the hook should start with "use"
function useTodos() {
    const [todos, setTodos] = useState([]);

    useEffect(() => {
        const uri = "https://sum-server.100xdevs.com/todos";
        axios.get(uri).then((res) => setTodos(res.data.todos));

        setInterval(() => {
            axios.get(uri).then((res) => setTodos(res.data.todos));
        }, 10000);
    }, []);

    return todos;
}

export default function Custom() {
    const todos = useTodos();

    return (
        <div>
            <Card>
                {todos.map((todo, idx) => (
                    <Todo
                        key={idx}
                        title={todo.title}
                        description={todo.description}
                    />
                ))}
            </Card>
        </div>
    );
}
