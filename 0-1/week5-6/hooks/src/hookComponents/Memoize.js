import { useMemo, useState } from "react";

export default function Memoize() {
    const [n, setN] = useState(0);
    const [count, setCount] = useState(0);
    const changeN = (e) => {
        setN(parseInt(e.target.value));
    };

    // bad approach, becuase 2 re-renders occur
    // const [sumValue, setSumValue] = useState(0)
    // useEffect(() => {
    //     let sum = 0;
    //     for (let i = 1; i <= n; i += 1) sum += i;
    //     setSumValue(sum)
    // }, [n])

    // good approach, only 1 re-render occurs
    let sumValue = useMemo(() => {
        let sum = 0;
        for (let i = 1; i <= n; i += 1) sum += i;
        return sum;
    }, [n]);

    const increaseCount = () => {
        setCount(count + 1);
    };

    const mainStyles = {
        display: "flex",
        flexDirection: "column",
        padding: "10rem",
        gap: "10px",
    };

    return (
        <div style={mainStyles}>
            <input type="number" value={n} onChange={changeN}></input>
            Sum is: {sumValue}
            <button onClick={increaseCount}>Count: {count}</button>
        </div>
    );
}
