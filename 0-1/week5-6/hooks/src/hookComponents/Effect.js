import { useState, useEffect } from "react";
import axios from "axios";
import Todo from "../components/Todo";
import Card from "../components/Card";

export default function Effect() {
    const [todos, setTodos] = useState([]);
    const [currentId, setCurrentId] = useState(1);

    // gets called only once becuase of the empty dependency array
    useEffect(() => {
        const uri = "https://sum-server.100xdevs.com/todos";
        // initial fetch of todos
        axios.get(uri).then((res) => setTodos(res.data.todos));

        setInterval(() => {
            axios.get(uri).then((res) => setTodos(res.data.todos));
        }, 10000);
    }, []);

    return (
        <div>
            <Card>
                <button onClick={() => setCurrentId(1)}>1</button>
                <button onClick={() => setCurrentId(2)}>2</button>
                <button onClick={() => setCurrentId(3)}>3</button>
                <button onClick={() => setCurrentId(4)}>4</button>
                <Todo id={currentId} />
            </Card>
            <Card>
                {todos.map((todo, idx) => (
                    <Todo
                        key={idx}
                        title={todo.title}
                        description={todo.description}
                    />
                ))}
            </Card>
        </div>
    );
}
