import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Gsn from "./components/Gsn";
import Todo from "./components/Todo";
import CountContext from "./context/CountContext";
import { useReducer } from "react";

function Home() {
    return <>Home</>;
}

function stateMachine(state, action) {
    switch (action.type) {
        case "increaseCount":
            return { ...state, count: state.count + 1 };
        case "decreaseCount":
            return { ...state, count: state.count - 1 };
        default:
            return state;
    }
}

function App() {
    const [state, dispatch] = useReducer(stateMachine, {
        count: 0,
    });

    return (
        <Router>
            <CountContext.Provider value={{ state, dispatch }}>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/gsn" element={<Gsn />} />
                    <Route path="/todo" element={<Todo />} />
                </Routes>
            </CountContext.Provider>
        </Router>
    );
}

export default App;
