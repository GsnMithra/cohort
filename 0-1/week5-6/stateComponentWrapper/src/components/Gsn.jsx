import { useState } from "react";
import { useContext, memo } from "react";
import CountContext from "../context/CountContext";

const Heading = (props) => {
    const [age, setAge] = useState(18);
    const { state, dispatch } = useContext(CountContext);

    const dispatchEvent = () => {
        dispatch({ type: "increaseCount" });
    };

    const incrementAge = () => {
        setAge(age + 1);
    };

    return (
        <div>
            <button onClick={incrementAge}>Age: {age}</button>
            <button onClick={dispatchEvent}>
                Change global state: {state.count}
            </button>
            <h1>{props.name}</h1>
        </div>
    );
};

const OptimizedHeader = memo((props) => {
    return <div>{props.name}</div>;
});

function Gsn() {
    const [name, setName] = useState("");
    const [nameSecond, setNameSecond] = useState("");

    const updateName = () => {
        setName(`my name is ${parseFloat(Math.random())}`);
    };

    const updateNameSecond = () => {
        setNameSecond(`my second name is ${parseFloat(Math.random())}`);
    };

    return (
        <div>
            <button
                onClick={updateNameSecond}
                style={{
                    margin: "10px",
                }}
            >
                Click me second
            </button>
            <button
                onClick={updateName}
                style={{
                    margin: "10px",
                }}
            >
                Click me
            </button>
            <Heading name={name} />
            <Heading name={"my name is mithra"} />
            <Heading name={"my name is gsn"} />
            <OptimizedHeader name={name} />
            <OptimizedHeader name={"mithra"} />
            <OptimizedHeader name={nameSecond} />
            <OptimizedHeader name={"gsn"} />
            <OptimizedHeader name={name} />
            <OptimizedHeader name={"gsn"} />
            <OptimizedHeader name={nameSecond} />
        </div>
    );
}

export default Gsn;
