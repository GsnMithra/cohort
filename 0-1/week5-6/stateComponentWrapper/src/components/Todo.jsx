import { useState } from "react";
import Card from "./Card";

const TodoEntry = (props) => {
    return (
        <div>
            <h2>{props.title}</h2>
            <h3>{props.description}</h3>
        </div>
    );
};

const TodoInput = (props) => {
    const handleTitleChange = (e) => {
        props.todoHandler({ ...props.todo, title: e.target.value });
    };

    const handleDescriptionChange = (e) => {
        props.todoHandler({ ...props.todo, description: e.target.value });
    };

    return (
        <>
            <input
                type="text"
                placeholder="title"
                value={props.todo.title}
                onChange={handleTitleChange}
            ></input>
            <input
                type="text"
                placeholder="description"
                value={props.todo.description}
                onChange={handleDescriptionChange}
            ></input>
            <button onClick={props.handleSubmit}>Add todo</button>
        </>
    );
};

const TodoList = (props) => {
    return (
        <div>
            <h1>Todos:</h1>
            {props.todos.map((todo, id) => (
                <TodoEntry
                    key={id}
                    title={todo.title}
                    description={todo.description}
                />
            ))}
        </div>
    );
};

export default function Todo() {
    const [todos, setTodos] = useState([]);
    const [todo, setTodo] = useState({
        title: "",
        description: "",
    });

    const handleSubmit = () => {
        if (todo.title === "" || todo.description === "") return;
        setTodos([...todos, todo]);
        setTodo({
            title: "",
            description: "",
        });
    };

    return (
        <div>
            <div>
                <Card>
                    <TodoList todos={todos} />
                </Card>
                <Card>
                    <TodoInput
                        todo={todo}
                        todoHandler={setTodo}
                        handleSubmit={handleSubmit}
                    />
                </Card>
            </div>
        </div>
    );
}
