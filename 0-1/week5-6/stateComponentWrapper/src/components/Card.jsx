const cardStyles = {
    border: "1px solid black",
    backgroundColor: "white",
    padding: "5px",
    boxShadow: "0px 4px 8px rgba(0, 0, 0, 0.1)",
    margin: "1rem",
    borderRadius: "0.5rem",
};

const Card = ({ children }) => {
    return <div style={cardStyles}>{children}</div>;
};

export default Card;
