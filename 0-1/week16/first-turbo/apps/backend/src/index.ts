import express from "express";
const cors = require('cors')
const app = express()

app.use(cors())
app.get('/', (req: any, res: any) => {
    const name = req.query.name

    return res.json({
        msg: `Hello, ${name}`
    })
})

app.post('/', async (req, res) => {
    const { username, password } = await req.body.json()

    return res.json({
        username,
        password
    })
})

app.listen(5000, () => {
    console.log('Server running on 5000')
})
