import './App.css'
import { useBackend } from './hooks'

function App() {
    const { loading, response } = useBackend()

    if (loading)
        return <div>Loading...</div>

    return (
        <div className="h-screen w-screen m-5 text-2xl border border-black rounded-full flex items-center justify-center">{JSON.stringify(response)}</div>
    )
}

export default App
