import axios from "axios"
import { useEffect, useState } from "react"

export const useBackend = () => {
    const [response, setResponse] = useState<any>()
    const [loading, setLoading] = useState<boolean>(true)

    useEffect(() => {
        axios.get("http://localhost:5000?name=Mithra")
            .then(res => {
                console.log(res.data)
                setResponse(res.data)
                setLoading(false)
            })
    }, [])

    return {
        loading,
        response
    }
}
