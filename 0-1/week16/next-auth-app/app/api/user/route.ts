import { nextAuthConfig } from "@/app/lib/nextAuthConfig";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";

export async function GET() {
    const { user } = await getServerSession(nextAuthConfig)
    return NextResponse.json({
        user
    })
}
