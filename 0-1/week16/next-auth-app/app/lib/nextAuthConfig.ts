import CredentialsProvider from "next-auth/providers/credentials"
import GoogleProvider from "next-auth/providers/google"

export const nextAuthConfig = {
    providers: [
        CredentialsProvider({
            name: "Credentials",
            credentials: {
                email: { label: 'email', placeholder: 'email', type: 'text' },
                password: { label: 'password', placeholder: 'password', type: 'password' }
            },
            async authorize(credentials: any, req: any) {
                const { email, password } = credentials

                return {
                    id: 2,
                    name: "Gsn Mithra",
                    email,
                }
            }
        }),
        GoogleProvider({
            clientId: process.env.GOOGLE_CLIENT_ID ?? "",
            clientSecret: process.env.GOOGLE_SECRET ?? ""
        })
    ],
    secret: process.env.NEXTAUTH_SECRET,
    callbacks: {
        jwt: async ({ token }: { token: any }) => {
            console.log(token);
            return token
        },
        session: async ({ session, token }: { session: any, token: any }) => {
            session.user.id = token.sub
            return session;
        }
    }
}
