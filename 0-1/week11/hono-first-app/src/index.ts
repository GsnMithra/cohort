import { Hono } from 'hono'

const app = new Hono()

async function testMiddleware(c: any, next: any) {
    console.log(`${c.req.query('name')}`)
    console.log('Middleware called')

    await next()
}

// this can also be used
// app.use(testMiddleware)

app.get('/', testMiddleware, async (c) => {
    const name = c.req.query('name')
    console.log('Service body called')

    return c.json({
        message: `Hello, ${name === undefined ? "World" : name}!`
    })
})

export default app
