import express from 'express';

const app = express();
let visitorCount = 0;

app.use(express.json())
app.use((req, res, next) => {
    visitorCount += 1;
    next()
})

app.get('/health', (req: any, res: any) => {
    return res.json({
        message: "Healthy!"
    })
})

app.get('/user/:roll', (req: any, res: any) => {
    const name = req.body.name
    const age = req.query.age
    const roll = req.params.roll

    return res.json({
        name,
        age,
        roll
    })
})

const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Server running on port: ${PORT}`)
})
