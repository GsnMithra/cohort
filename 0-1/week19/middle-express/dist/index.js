"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const app = (0, express_1.default)();
let visitorCount = 0;
app.use(express_1.default.json());
app.use((req, res, next) => {
    visitorCount += 1;
    next();
});
app.get('/health', (req, res) => {
    return res.json({
        message: "Healthy!"
    });
});
app.get('/user/:roll', (req, res) => {
    const name = req.body.name;
    const age = req.query.age;
    const roll = req.params.roll;
    console.log(name, age, roll);
    return res.json({
        name,
        age,
        roll
    });
});
const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Server running on port: ${PORT}`);
});
