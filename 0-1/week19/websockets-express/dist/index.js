"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ws_1 = require("ws");
const express_1 = __importDefault(require("express"));
const app = (0, express_1.default)();
const PORT = 8080;
const server = app.listen(PORT, () => console.log(`Server started on port ${PORT}!`));
const wss = new ws_1.WebSocketServer({ server });
wss.on('connection', (socket) => {
    socket.on('message', (message, isBinary) => {
        wss.clients.forEach(client => {
            if (client !== socket && client.readyState === ws_1.WebSocket.OPEN)
                client.send(message, { binary: isBinary });
        });
    });
    socket.send("Connection established!");
});
