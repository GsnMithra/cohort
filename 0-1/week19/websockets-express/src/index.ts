import { WebSocket, WebSocketServer } from "ws";
import express from "express"

const app = express();
const PORT = 8080
const server = app.listen(PORT, () => console.log(`Server started on port ${PORT}!`));

const wss = new WebSocketServer({ server });
wss.on('connection', (socket: WebSocket) => {
    socket.on('message', (message: string, isBinary: boolean) => {
        wss.clients.forEach(client => {
            if (client !== socket && client.readyState === WebSocket.OPEN)
                client.send(message, { binary: isBinary });
        })
    })

    socket.send("Connection established!");
})
