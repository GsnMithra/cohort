import { useEffect, useState } from 'react'
import './App.css'

function App() {
    const [socket, setSocket] = useState<WebSocket | null>(null);
    const [messages, setMessages] = useState<string[]>([])
    const [message, setMessage] = useState<string>("")

    const sendMessage = () => {
        if (socket) {
            socket.send(message)
            setMessage("")
        }
    }

    useEffect(() => {
        const socket = new WebSocket("ws://localhost:8080")
        socket.onopen = () => {
            setSocket(socket)
        }

        socket.onmessage = (event) => {
            console.log(event.data)
            setMessages([...messages, event.data])
        }

        return () => {
            socket.close()
        }
    }, [])

    if (!socket)
        return <div>Connecting...</div>

    return (
        <>
            <input type="text" value={message} onChange={(e) => setMessage(e.target.value)} />
            <button onClick={sendMessage}>Send Message</button>
            <ul>
                {messages.map((message, index) => (
                    <li key={index}>{message}</li>
                ))}
            </ul>
        </>
    )
}

export default App
