import express from 'express';
import { createClient } from 'redis';

const app = express();
const client = createClient();
app.use(express.json());

app.post("/submit", async (req, res) => {
    const { id, username, problemId, code, language } = req.body;
    // would normally insert data into a database here

    try {
        await client.lPush("submissions", JSON.stringify({ id, username, problemId, language, code }));
        res.json({
            message: "Submitted!"
        })
    } catch (err) {
        res.status(500).json({
            message: "Error submitting"
        })
    }
})

const startServer = async () => {
    try {
        await client.connect();
        const PORT = 3000;
        app.listen(PORT, () => {
            console.log(`Server running on port: ${PORT}`);
        })
    } catch (error) {
        console.error("Error starting server", error);
    }
}

startServer()
