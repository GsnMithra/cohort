import { createClient } from "redis";

const client = createClient();

const initializeWorker = async () => {
    await client.connect();

    while (true) {
        const submission = await client.brPop("submissions", 0);
        console.log(JSON.parse(submission.element));
    }
}

initializeWorker()
