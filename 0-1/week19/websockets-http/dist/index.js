"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ws_1 = require("ws");
const http_1 = __importDefault(require("http"));
const server = http_1.default.createServer((req, res) => {
    res.end("hello there!");
});
const wss = new ws_1.WebSocketServer({ server });
wss.on('connection', (socket) => {
    socket.on('error', console.error);
    socket.on('message', (data, isBinary) => {
        wss.clients.forEach((client) => {
            if (client.readyState === ws_1.WebSocket.OPEN && client !== socket)
                client.send(data, { binary: isBinary });
        });
    });
    socket.send("Connection established..!");
});
const PORT = 8080;
server.listen(PORT, () => {
    console.log(`Server running on ${PORT}`);
});
