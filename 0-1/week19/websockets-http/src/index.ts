import { WebSocket, WebSocketServer } from "ws";
import http from "http"

const server = http.createServer((req, res) => {
    res.end("hello there!")
})

const wss = new WebSocketServer({ server })

wss.on('connection', (socket) => {
    socket.on('error', console.error)

    socket.on('message', (data, isBinary) => {
        wss.clients.forEach((client) => {
            if (client.readyState === WebSocket.OPEN && client !== socket)
                client.send(data, { binary: isBinary })
        })
    })

    socket.send("Connection established..!")
})

const PORT = 8080;
server.listen(PORT, () => {
    console.log(`Server running on ${PORT}`)
})
