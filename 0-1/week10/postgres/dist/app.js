"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const pg_1 = require("pg");
const client = new pg_1.Client({
    connectionString: "postgresql://admin:8jCvVor3WOcD@ep-flat-recipe-a1vi8z92.ap-southeast-1.aws.neon.tech/cohort?sslmode=require"
});
client.connect().then(() => console.log("Connected to the database"));
// for secured queries, do NOT use the user provided values directly, instead use client.query (q, values) syntax
const createTable = (tableName) => __awaiter(void 0, void 0, void 0, function* () {
    const query = `
        CREATE TABLE $1 (
            id SERIAL PRIMARY KEY,
            emailId VARCHAR(255) UNIQUE NOT NULL,
            password VARCHAR(255) NOT NULL,
            firstName VARCHAR(255) NOT NULL,
            lastName VARCHAR(255) NOT NULL
        );`;
    const results = yield client.query(query, [tableName]);
    console.log(results);
});
const dropTable = (tableName) => __awaiter(void 0, void 0, void 0, function* () {
    const query = `
        DROP TABLE $1;
    `;
    const results = yield client.query(query, [tableName]);
    console.log(results);
});
const updateUser = (field, updatedValue) => __awaiter(void 0, void 0, void 0, function* () {
    const query = `
        UPDATE users
        SET $1 = '$2';
    `;
    const results = yield client.query(query, [field, updatedValue]);
    console.log(results);
});
const deleteUser = (fieldEntry, fieldValue) => __awaiter(void 0, void 0, void 0, function* () {
    const query = `
        DELETE from users
        where $1 = '$2';
    `;
    const results = yield client.query(query, [fieldEntry, fieldValue]);
    console.log(results);
});
const createNewUser = (user) => __awaiter(void 0, void 0, void 0, function* () {
    const query = "INSERT INTO users (emailid, password, firstname, lastname) VALUES ($1, $2, $3, $4);";
    const values = [user.emailId, user.password, user.firstName, user.lastName];
    const results = yield client.query(query, values);
    console.log(results);
});
//createTable("users")
//dropTable("users")
createNewUser({
    emailId: "mithragsn9@gmail.com",
    password: "mitra",
    firstName: "Gsn",
    lastName: "Mithra"
});
//updateUser("emailId", "mithragsn1@gmail.com")
//deleteUser("emailId", "mithragsn1@gmail.com")
