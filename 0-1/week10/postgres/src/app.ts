import { Client } from "pg"
import dotenv from 'dotenv';
dotenv.config();

type User = {
    emailId: string,
    password: string,
    firstName: string,
    lastName: string,
}

const client = new Client({
    connectionString: process.env.CONNECTION_STRING
})

client.connect().then(() => console.log("Connected to the database"))

// for secured queries, do NOT use the user provided values directly, instead use client.query (q, values) syntax
// if so used the prepared statement then, do NOT use '' quotes wrapping around the value

const createTable = async (tableName: string) => {
    const query: string = `
        CREATE TABLE $1 (
            id SERIAL PRIMARY KEY,
            emailId VARCHAR(255) UNIQUE NOT NULL,
            password VARCHAR(255) NOT NULL,
            firstName VARCHAR(255) NOT NULL,
            lastName VARCHAR(255) NOT NULL
        );`

    const results = await client.query(query, [tableName])
    console.log(results)
}

const dropTable = async (tableName: string) => {
    const query = `
        DROP TABLE $1;
    `;

    const results = await client.query(query, [tableName])
    console.log(results)
}

const updateUser = async (field: string, updatedValue: string) => {
    const query = `
        UPDATE users
        SET $1 = $2;
    `;

    const results = await client.query(query, [field, updatedValue])
    console.log(results)
}

const deleteUser = async (fieldEntry: string, fieldValue: string) => {
    const query = `
        DELETE from users
        where $1 = $2;
    `;

    const results = await client.query(query, [fieldEntry, fieldValue])
    console.log(results)
}

const createNewUser = async (user: User) => {
    const query = "INSERT INTO users (emailid, password, firstname, lastname) VALUES ($1, $2, $3, $4);";

    const values: string[] = [user.emailId, user.password, user.firstName, user.lastName]
    const results = await client.query(query, values)
    console.log(results)
}

//createTable("users")
//dropTable("users")
//createNewUser({
//    emailId: "mithragsn9@gmail.com",
//    password: "mitra",
//    firstName: "Gsn",
//    lastName: "Mithra"
//})
//updateUser("emailId", "mithragsn1@gmail.com")
//deleteUser("emailId", "mithragsn1@gmail.com")

//also close the connection using
//client.close()
