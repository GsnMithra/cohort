"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("@prisma/client");
const prisma = new client_1.PrismaClient();
const createUser = (email, password, firstName, lastName) => __awaiter(void 0, void 0, void 0, function* () {
    yield prisma.user.create({
        data: {
            email,
            password,
            firstName,
            lastName
        }
    }).then((res) => {
        console.log(res);
    }).catch((err) => {
        console.log(err);
    });
});
const getAllUsers = () => __awaiter(void 0, void 0, void 0, function* () {
    yield prisma.user.findMany({})
        .then((res) => {
        console.log(res);
    });
});
const updateUser = ({ email, firstName, lastName }) => __awaiter(void 0, void 0, void 0, function* () {
    yield prisma.user.update({
        where: {
            email: email
        },
        data: {
            firstName,
            lastName
        }
    }).then((res) => {
        console.log(res);
    }).catch((err) => {
        console.log(err);
    });
});
//createUser("mithragsn1@gmail.com", "mitra", "Gsn", "Mithra")
//getAllUsers();
//updateUser({ email: "mithragsn@gmail.com", firstName: "Gsn", lastName: "Mithra" })
getAllUsers();
