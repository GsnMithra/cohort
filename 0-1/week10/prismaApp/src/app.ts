import { PrismaClient } from "@prisma/client"
import { create } from "domain"
const prisma = new PrismaClient()

const createUser = async (email: string, password: string, firstName: string, lastName: string) => {
    await prisma.user.create({
        data: {
            email,
            password,
            firstName,
            lastName
        }
    }).then((res) => {
        console.log(res)
    }).catch((err) => {
        console.log(err)
    })
}

const getAllUsers = async () => {
    await prisma.user.findMany({})
        .then((res) => {
            console.log(res)
        })
}

type UpdateParamsType = {
    email: string,
    firstName: string,
    lastName: string
}

const updateUser = async ({ email, firstName, lastName }: UpdateParamsType) => {
    await prisma.user.update({
        where: {
            email: email
        },
        data: {
            firstName,
            lastName
        }
    }).then((res) => {
        console.log(res)
    }).catch((err) => {
        console.log(err)
    })
}

//createUser("mithragsn1@gmail.com", "mitra", "Gsn", "Mithra")
//getAllUsers();
//updateUser({ email: "mithragsn@gmail.com", firstName: "Gsn", lastName: "Mithra" })
getAllUsers();
