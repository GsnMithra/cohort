import { Client } from "pg"

const client = new Client({
    host: "localhost",
    port: 5455,
    database: "cohort",
    user: "cohort",
    password: "password"
})

client.connect();

const getAllUsers = async () => {
    const queryString = `
        SELECT * FROM users;
    `

    const res = await client.query(queryString)
    console.log(`Number of rows: ${res.rowCount}`)
    console.log(res.rows)
}

getAllUsers();
