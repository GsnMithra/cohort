import { useRef, useState } from "react";

function LoginOTPDesign() {
    const [otpFields, setOtpFields] = useState(["", "", "", ""]);
    const otpFieldRefs = [useRef(), useRef(), useRef(), useRef()];

    const handleOTPChange = (e, index) => {
        const newOtpFields = [...otpFields];
        newOtpFields[index] = e.target.value;
        setOtpFields(newOtpFields);

        if (e.target.value && index < 3) {
            otpFieldRefs[index + 1].current.focus();
        }
    };

    return (
        <div
            style={{
                width: "100vw",
                height: "100vh",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                gap: "25px",
            }}
        >
            {otpFields.map((field, index) => (
                <input
                    key={index}
                    type="number"
                    ref={otpFieldRefs[index]}
                    style={{
                        width: "35px",
                        height: "50px",
                        padding: "0px",
                        fontSize: "2rem",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        paddingLeft: "15px",
                    }}
                    value={field === "" ? undefined : parseInt(field)}
                    onChange={(e) => handleOTPChange(e, index)}
                    onKeyDown={(e) => {
                        if (e.key === "Backspace") {
                            if (index - 1 >= 0)
                                otpFieldRefs[index - 1].current.focus();
                        }
                    }}
                    maxLength={1}
                />
            ))}
        </div>
    );
}

export default LoginOTPDesign;
