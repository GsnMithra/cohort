import { useState, useEffect } from "react";
import axios from "axios";

const ProfileWrapper = ({
    profilePicture,
    name,
    age,
    location,
    followers,
    likes,
    photos,
    githubId = undefined,
}) => {
    const [userDetails, setUserDetails] = useState({
        name,
        age,
        profilePicture,
        location,
        followers,
        likes,
        photos,
    });

    useEffect(() => {
        if (!githubId) return;
        const uri = `https://api.github.com/users/${githubId}`;
        async function getGithubDetails() {
            axios.get(uri).then((response) => {
                const userData = response.data;
                setUserDetails({
                    name: userData.name,
                    followers: userData.followers,
                    profilePicture: userData.avatar_url,
                    age: 20,
                    location: "Hyderabad",
                    likes: userData.following,
                    photos: 3,
                });
            });
        }

        getGithubDetails();
    }, [githubId]);

    const profileStyles = {
        height: "700px",
        width: "500px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        position: "relative",
        border: "0.5px solid black",
        borderRadius: "3rem",
    };

    const backgroundDiv = {
        borderTopRightRadius: "3rem",
        borderTopLeftRadius: "3rem",
        height: "40%",
        width: "100%",
        backgroundColor: `rgb(${parseInt(Math.random() * 255)}, ${parseInt(Math.random() * 255)}, ${parseInt(Math.random() * 255)})`,
    };

    const mainContent = {
        borderBottomRightRadius: "3rem",
        borderBottomLeftRadius: "3rem",
        height: "60%",
        width: "100%",
        backgroundColor: `rgb(${parseInt(Math.random() * 255)}, ${parseInt(Math.random() * 255)}, ${parseInt(Math.random() * 255)})`,
    };

    return (
        <div style={profileStyles}>
            <div
                style={{
                    position: "absolute",
                    transform: "translate(-50px, -50px)",
                    top: "40%",
                    left: "50%",
                }}
            >
                <img
                    src={userDetails.profilePicture}
                    alt=""
                    height="100px"
                    width="100px"
                />
            </div>
            <div style={backgroundDiv}></div>
            <div style={mainContent}>
                <div
                    style={{
                        width: "100%",
                        height: "65%",
                        alignItems: "center",
                        justifyContent: "center",
                        display: "flex",
                        borderBottom: "1px solid black",
                        flexDirection: "column",
                        gap: "30px",
                    }}
                >
                    <div
                        style={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            flexDirection: "row",
                            gap: "15px",
                        }}
                    >
                        <div>{userDetails.name}</div>
                        <div>{userDetails.age}</div>
                    </div>
                    <div>{userDetails.location}</div>
                </div>
                <div
                    style={{
                        width: "100%",
                        height: "35%",
                        alignItems: "center",
                        justifyContent: "space-evenly",
                        display: "flex",
                        flexDirection: "row",
                    }}
                >
                    <div>{userDetails.followers}</div>
                    <div>{userDetails.likes}</div>
                    <div>{userDetails.photos}</div>
                </div>
            </div>
        </div>
    );
};

export default ProfileWrapper;
