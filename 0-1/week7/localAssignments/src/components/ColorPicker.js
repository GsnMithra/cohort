import { useState } from "react";

const ColorPicker = () => {
    const [color, setColor] = useState("red");

    return (
        <div
            style={{
                padding: "0px",
                margin: "0px",
                width: "100vw",
                height: "100vh",
                backgroundColor: color,
            }}
        >
            <div
                style={{
                    display: "flex",
                    alignItems: "center",
                    position: "absolute",
                    bottom: "1%",
                    left: "50%",
                    gap: "10rem",
                    transform: "translate(-50px, -50px)",
                }}
            >
                <div
                    onClick={() => setColor("red")}
                    style={{
                        cursor: "pointer",
                    }}
                >
                    Red
                </div>
                <div
                    onClick={() => setColor("green")}
                    style={{
                        cursor: "pointer",
                    }}
                >
                    Green
                </div>
                <div
                    onClick={() => setColor("blue")}
                    style={{
                        cursor: "pointer",
                    }}
                >
                    Blue
                </div>
            </div>
        </div>
    );
};

export default ColorPicker;
