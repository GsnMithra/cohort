import { BrowserRouter, Routes, Route } from "react-router-dom";
import ProfileWrapper from "./components/ProfileWrapper";
import ColorPicker from "./components/ColorPicker";
import LoginOTPDesign from "./components/LoginOTPDesign";

function App() {
    const routes = [
        {
            path: "/profile",
            element: (
                <ProfileWrapper
                    profilePicture=""
                    name="Mithra"
                    age={20}
                    location="Hyderabad"
                    followers={30000}
                    likes={60000}
                    photos={3}
                />
            ),
        },
        {
            path: "/githubprofile",
            element: <ProfileWrapper githubId="GsnMithra" />,
        },
        {
            path: "/colorpicker",
            element: <ColorPicker />,
        },
        {
            path: "/loginotp",
            element: <LoginOTPDesign />,
        },
    ];

    return (
        <BrowserRouter>
            <Routes>
                {routes.map((route, idx) => (
                    <Route
                        key={idx}
                        path={route.path}
                        element={route.element}
                    />
                ))}
            </Routes>
        </BrowserRouter>
    );
}

export default App;
