import * as React from "react";
import { CountContext } from "./appContexts";
import { useContext } from "react";

function App() {
    const [count, setCount] = React.useState(0);
    const appStyles = {
        padding: "20px",
        margin: "15px",
    };

    return (
        <div style={appStyles}>
            <CountContext.Provider value={{ count, setCount }}>
                <Count />
            </CountContext.Provider>
        </div>
    );
}

function Count() {
    const { count } = useContext(CountContext);

    const countStyles = {
        padding: "10px",
    };

    return (
        <div>
            <div style={countStyles}>Count: {count}</div>
            <Buttons />
        </div>
    );
}

function Buttons() {
    const { setCount } = useContext(CountContext);

    const incrementCount = () => {
        setCount((a) => a + 1);
    };

    const decrementCount = () => {
        setCount((a) => a - 1);
    };

    const buttonStyles = {
        margin: "5px",
    };

    return (
        <div>
            <button style={buttonStyles} onClick={incrementCount}>
                increment
            </button>
            <button style={buttonStyles} onClick={decrementCount}>
                decrement
            </button>
        </div>
    );
}

export default App;
