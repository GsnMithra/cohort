import * as React from "react";

function App() {
    const [count, setCount] = React.useState(0);
    const appStyles = {
        padding: "20px",
        margin: "15px",
    };

    return (
        <div style={appStyles}>
            <Count count={count} setCount={setCount} />
        </div>
    );
}

function Count({ count, setCount }) {
    const countStyles = {
        padding: "10px",
    };

    return (
        <div>
            <div style={countStyles}>Count: {count}</div>
            <Buttons handler={setCount} />
        </div>
    );
}

function Buttons({ handler }) {
    const incrementCount = () => {
        handler((a) => a + 1);
    };

    const decrementCount = () => {
        handler((a) => a - 1);
    };

    const buttonStyles = {
        margin: "5px",
    };

    return (
        <div>
            <button style={buttonStyles} onClick={incrementCount}>
                increment
            </button>
            <button style={buttonStyles} onClick={decrementCount}>
                decrement
            </button>
        </div>
    );
}

export default App;
