import { atomFamily } from "recoil";
import { todoList } from "./todos";

export const todosAtomFamily = atomFamily({
    key: "todosAtomFamily",
    default: (id) => {
        return todoList.find((x) => x.id === id);
    },
});
