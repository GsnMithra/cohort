import { atom, atomFamily, selector, selectorFamily } from 'recoil';
import { todos } from './todos';

// this type of approach is unoptimal and not recommended
export const todosAtom = atom({
    key: "todosAtom",
    default: []
})

// this type of approach is optimal and recommended
export const todosAtoms = atomFamily({
    key: "atomsFamily",
    default: {
        id: 0,
        title: "",
        description: ""
    }
})
