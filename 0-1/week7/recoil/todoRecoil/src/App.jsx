import { RecoilRoot, useRecoilState, useRecoilValue } from "recoil"
import { todosAtom } from "./atoms"
import { memo, useState } from "react"

function App() {
    return (
        <RecoilRoot>
            <Todos />
        </RecoilRoot>
    )
}

const Todos = () => {
    const [todos, setTodos] = useRecoilState(todosAtom)
    const [todo, setTodo] = useState({
        id: "",
        title: "",
        description: ""
    })

    const updateTodos = () => {
        setTodo({
            id: Date.now(),
            ...todo
        })

        setTodos({
            ...todos,
            todo
        })
    }

    return (
        <div>
            <div>
                <input type="text" value={todo.title} onChange={(e) => {
                    setTodo({ ...todo, title: e.target.value })
                }}></input>
                <input type="text" value={todo.description} onChange={(e) => {
                    setTodo({ ...todo, description: e.target.value })
                }}></input>
                <button onClick={updateTodos}>Add todo</button>
                <TodoRenderer />
            </div>
        </div>
    )
}

const TodoRenderer = memo(() => {
    // const todos = useRecoilValue(todosAtom) // using an atom
    const todos = useRecoilValue(todosAtom) // using an atomFamily

    return (
        <>
            {
                todos.map((todo, key) => (
                    <Todo key={key} id={todo.id} />
                ))
            }
        </>
    )
})

const Todo = memo(({ id }) => {
    const todo = useRecoilValue(todosAtom(id))

    return <div style={{
        margin: "10px"
    }}>
        <h3>Title: {todo.title}</h3>
        <h5>Description: {todo.description}</h5>
    </div>
})

export default App
