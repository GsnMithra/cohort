import { atom, selector } from "recoil";
import axios from "axios";

// export const notification = atom({
//     key: "notification",
//     default: {
//         jobs: 0,
//         network: 0,
//         something: 0,
//     },
// });
// and then use "useEffect() in your component to fetch the data and update the atom"
// bad practice, use selector instead

// selector is a read-only atom that can be used to derive a value based on other atoms or selector
// async data fetching with recoil
export const notifications = atom({
    key: "networkAtom",
    default: selector({
        key: "networkAtomDefault",
        get: async () => {
            return await axios
                .get("https://sum-server.100xdevs.com/notifications")
                .then((response) => response.data);
        },
    }),
});

export const totalNotificationSelector = selector({
    key: "totalNotificationSelector",
    get: ({ get }) => {
        const allNotifications = get(notifications);
        return (
            allNotifications.network +
            allNotifications.jobs +
            allNotifications.notifications +
            allNotifications.messaging
        );
    },
});
