import { atom, selector } from "recoil";

// just a normal useState equivalent in Recoil
export const countAtom = atom({
    key: "countAtom",
    default: 0,
});

// selector is a derived state from the atom equivalent to useMemo
export const evenSelector = selector({
    key: "evenSelector",
    get: (props) => {
        const count = props.get(countAtom);
        return count % 2 === 0;
    },
});

// dual derived state from the atom and another selector
export const evenAndGreaterEqualTen = selector({
    key: "evenAndGreaterEqualTen",
    get: ({ get }) => {
        const count = get(countAtom);

        // dual derivation
        const isEven = get(evenSelector);
        return isEven && count >= 10;
    },
});

// RecoilRoot
// atom() => used for defining the atom
// selector() => used for defining the derived state
// useRecoilValue() : returns just the value
// useRecoilState() : returns the value and the setValue function
// useSetRecoilState() : returns just the setValue function
