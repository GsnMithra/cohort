import * as React from "react";
import { RecoilRoot, useRecoilValue, useSetRecoilState } from "recoil";
import {
    countAtom,
    evenAndGreaterEqualTen,
    evenSelector,
} from "./store/atoms/count";

// atom()
// selector()
// useRecoilState()
// useRecoilValue()
// useSetRecoilState()
// RecoilRoot

function App() {
    const appStyles = {
        padding: "20px",
        margin: "15px",
    };

    return (
        <RecoilRoot>
            <div style={appStyles}>
                <Count />
            </div>
        </RecoilRoot>
    );
}

function Count() {
    const count = useRecoilValue(countAtom);

    const countStyles = {
        padding: "10px",
    };

    return (
        <div>
            <div style={countStyles}>Count: {count}</div>
            <Buttons />
            <EvenCountNotifier />
        </div>
    );
}

function EvenCountNotifier() {
    const isEven = useRecoilValue(evenSelector);
    const isEvenAndTen = useRecoilValue(evenAndGreaterEqualTen);

    if (isEven && isEvenAndTen)
        return <div>It is even and greater than or equal to 10</div>;
    else if (isEven) return <div>It is even</div>;
    return null;
}

function Buttons() {
    const setCountAtom = useSetRecoilState(countAtom);

    const buttonStyles = {
        margin: "5px",
    };

    return (
        <div>
            <button
                style={buttonStyles}
                onClick={() => setCountAtom((a) => a + 1)}
            >
                increment
            </button>
            <button
                style={buttonStyles}
                onClick={() => setCountAtom((a) => a - 1)}
            >
                decrement
            </button>
        </div>
    );
}

export default App;
