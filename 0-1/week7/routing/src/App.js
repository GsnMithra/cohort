// Suspense introduced because of lazy loading of components
// Landing and Dashboard components are now asynchronously loaded
import React, { Suspense } from "react";
import { BrowserRouter, Routes, Route, useNavigate } from "react-router-dom";

// normal imports
// import { Landing } from "./components/Landing";
// import { Dashboard } from "./components/Dashboard";

// lazy imports
const Landing = React.lazy(() => import("./components/Landing"));
const Dashboard = React.lazy(() => import("./components/Dashboard"));

const NavBar = React.memo(function () {
    const navigate = useNavigate();
    return (
        <nav>
            <ul>
                <li onClick={() => navigate("/")} style={{ cursor: "pointer" }}>
                    Home
                </li>
                <li
                    onClick={() => navigate("/landing")}
                    style={{ cursor: "pointer" }}
                >
                    Landing
                </li>
                <li
                    onClick={() => navigate("/dashboard")}
                    style={{ cursor: "pointer" }}
                >
                    Dashboard
                </li>
            </ul>
        </nav>
    );
});

function Loading() {
    return <div>Loading</div>;
}

function App() {
    const routes = [
        {
            path: "/dashboard",
            element: (
                <Suspense fallback={<Loading />}>
                    <Dashboard />
                </Suspense>
            ),
        },
        {
            path: "/landing",
            element: (
                <Suspense fallback={<Loading />}>
                    <Landing />
                </Suspense>
            ),
        },
    ];

    return (
        <BrowserRouter>
            <NavBar />
            <Routes>
                {routes.map((route, idx) => (
                    <Route
                        key={idx}
                        path={route.path}
                        element={route.element}
                    />
                ))}
            </Routes>
        </BrowserRouter>
    );
}

export default App;
