import { useState } from "react"
import axios from "axios"
import { configuration } from "../config"
import { useNavigate } from "react-router-dom"
import { BackButton } from "../components/BackButton"

export const CreateBlog = () => {
    const [title, setTitle] = useState("")
    const [content, setContent] = useState("")
    const navigate = useNavigate()

    const publishBlog = async () => {
        if (title === "" || content === "")
            return

        console.log(localStorage.getItem("token"))

        await axios.post(`${configuration.backendURL}/api/v1/blog`, {
            title,
            content
        }, {
            headers: {
                Authorization: localStorage.getItem("token") ?? ""
            }
        }).then(res => {
            console.log(res.data)
            navigate(`/blog/${res.data.blogId}`)
        })
    }

    return (
        <div className="grid grid-rows-10 h-screen w-screen pl-80 pr-80 pt-28 items-center justify-start">
            <div className="absolute top-[3%] left-[2%]">
                <BackButton height={40} width={40} onClick={() => navigate('/')} />
            </div>
            <div
                className="absolute top-[2%] right-[2%] bg-green-600 p-2 rounded-2xl text-white text-sm shadow-xl cursor-pointer"
                onClick={publishBlog}
            >
                Publish
            </div>
            <textarea placeholder="Title" className="text-[3.5rem] outline-none font-serif" value={title} onChange={(e) => setTitle(e.target.value)} />
            <textarea placeholder="Tell your story..." className="text-lg outline-none font-serif"
                value={content}
                onChange={(e) => setContent(e.target.value)}
                style={{
                    width: '100%',
                    overflow: 'hidden',
                    resize: "vertical"
                }}
            />
        </div>
    )
}
