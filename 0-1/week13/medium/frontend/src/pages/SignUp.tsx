import { Quote } from "../components/Quote"
import { Auth } from "../components/Auth"

export const SignUp = () => {
    return <div className="grid grid-cols-1 lg:grid-cols-2 h-screen w-screen">
        <div className="flex items-center justify-center">
            <Auth signMode="signup" />
        </div>

        <div className="hidden lg:block">
            <Quote />
        </div>
    </div>
}
