import { useState } from "react"
import PlusIco from "../assets/plus.svg"
import { useNavigate } from "react-router-dom"
import { useBlogs } from "../hooks"
import { BlogDisplay } from "../components/BlogDisplay"

export const Blogs = () => {
    const navigate = useNavigate()
    const [blogType, setBlogType] = useState<"all" | "following">("all")
    const { loading, blogs } = useBlogs()

    console.log(blogs)

    if (loading) {
        return (<div className="flex items-center justify-center h-screen w-screen">
            <div className="relative w-8 h-8 rounded-full animate-rotate">
                <div className="absolute inset-0 border-4 border-black rounded-full animate-prixClipFix"></div>
            </div>
        </div>)
    }

    return (
        <div className="grid grid-rows-10 h-screen w-screen pl-80 pr-80">
            <div className="row-span-1 border-b-[1.5px] border-gray-200 items-center justify-start flex p-8 pb-5 m-10 gap-8">
                <img
                    src={PlusIco}
                    alt="Plus"
                    height={35}
                    width={35}
                    className="mb-5 cursor-pointer"
                    onClick={() => navigate('/create-blog')}
                />
                <div className="text-sm cursor-pointer font-light pb-5"
                    style={blogType === "all" ? {
                        fontWeight: 450,
                        borderBottom: "1px solid black",
                    } : undefined}
                    onClick={() => setBlogType("all")}
                >
                    For you
                </div>
                <div className="text-sm cursor-pointer font-light pb-5"
                    style={blogType === "following" ? {
                        fontWeight: 450,
                        borderBottom: "1px solid black",
                    } : undefined}
                    onClick={() => setBlogType("following")}
                >
                    Following
                </div>
            </div>
            <div className="row-span-9 p-10 flex flex-col gap-8 justify-center items-start m-10">
                {
                    blogs?.map((blog, ix) => (
                        <BlogDisplay
                            authorName={blog.user.name}
                            uploadedAt={new Date(blog.uploadedAt).toLocaleDateString('en-US', {
                                year: 'numeric',
                                month: 'long',
                                day: 'numeric'
                            })}
                            title={blog.title}
                            description={blog.content} />
                    ))
                }
            </div>
        </div>
    )
}
