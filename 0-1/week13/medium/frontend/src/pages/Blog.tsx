import { useNavigate, useParams } from "react-router-dom"
import { useBlog } from "../hooks"
import { BackButton } from "../components/BackButton"
import Avatar from "../assets/avatar.png"

export const Blog = () => {
    const navigate = useNavigate()
    const { blogId } = useParams<{ blogId: string }>()
    const { loading, blog } = useBlog(blogId ?? "")

    if (loading) {
        return (<div className="flex items-center justify-center h-screen w-screen">
            <div className="relative w-8 h-8 rounded-full animate-rotate">
                <div className="absolute inset-0 border-4 border-black rounded-full animate-prixClipFix"></div>
            </div>
        </div>)
    }

    return (
        <div className="grid grid-cols-10 h-screen w-screen">
            <div className="absolute top-[3%] left-[2%]">
                <BackButton height={40} width={40} onClick={() => navigate('/')} />
            </div>
            <div className="col-span-10 lg:col-span-8 pl-24 pt-24 pr-24">
                <div className="text-3xl font-extrabold pb-1">{blog?.title}</div>
                <div className="text-sm opacity-45 pb-5">Posted on {new Date(blog?.uploadedAt).toLocaleDateString('en-US', {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric'
                })
                }</div>
                <div className="opacity-70 font-semibold">{blog?.content}</div>
            </div>
            <div className="hidden lg:block w-full lg:col-span-2 pl-10 pt-10 pr-10">
                <div className="flex flex-col items-start justify-center gap-2">
                    <div className="text-sm font-bold">Author</div>

                    <div className="flex flex-row items-start gap-2">
                        <div className="w-11 h-11 m-1 ml-0">
                            <img src={Avatar} alt="Avatar" />
                        </div>
                        <div className="flex flex-col items-start justify-center">
                            <div className="text-lg font-bold">{blog?.user.name}</div>
                            <div className="text-wrap text-sm opacity-45">AI Enthusiast</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
