import './App.css'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { SignIn } from './pages/SignIn'
import { SignUp } from './pages/SignUp'
import { Blog } from './pages/Blog'
import { Blogs } from './pages/Blogs'
import { CreateBlog } from './pages/CreateBlog'

function App() {
    return (
        <Router>
            <Routes>
                <Route path='/signup' element={<SignUp />} />
                <Route path='/signin' element={<SignIn />} />
                <Route path='/blog/:blogId' element={<Blog />} />
                <Route path='/' element={<Blogs />} />
                <Route path='/create-blog' element={<CreateBlog />} />
            </Routes>
        </Router>
    )
}

export default App
