import { useState, useEffect } from "react"
import { configuration } from "../config"
import axios from "axios"

export interface Blog {
    id: string,
    title: string,
    user: {
        id: string,
        name: string,
        bio: string
    },
    uploadedAt: string,
    content: string
}

export function useBlog(blogId: string) {
    const [blog, setBlog] = useState<Blog>()
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        axios.get(`${configuration.backendURL}/api/v1/blog/${blogId}`, {
            headers: {
                Authorization: localStorage.getItem("token")
            }
        }).then(res => {
            setBlog(res.data)
            setLoading(false)
        })
    }, [blogId])

    return {
        loading,
        blog
    }
}

export function useBlogs() {
    const [blogs, setBlogs] = useState<Blog[]>()
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        axios.get(`${configuration.backendURL}/api/v1/blog`, {
            headers: {
                Authorization: localStorage.getItem("token")
            }
        }).then(res => {
            setBlogs(res.data)
            setLoading(false)
        })
    }, [])

    return {
        loading,
        blogs
    }
}
