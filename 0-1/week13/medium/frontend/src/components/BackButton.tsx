import BackIco from "../assets/back.png"
import { MouseEventHandler } from "react"

export const BackButton = ({
    height,
    width,
    onClick
}: {
    height: number,
    width: number,
    onClick: MouseEventHandler<HTMLDivElement>
}) => {
    return (
        <div className="cursor-pointer border rounded-full shadow-lg" onClick={onClick}>
            <img src={BackIco} alt="Back" height={height} width={width} />
        </div>
    )
}
