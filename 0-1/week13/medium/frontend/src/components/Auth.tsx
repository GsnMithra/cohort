import { useState, ChangeEvent } from "react"
import { SignUpSchema } from "@gsnmithra/medium-types"
import { useNavigate } from "react-router-dom"
import { configuration } from "../config"
import axios from "axios"

interface InputBoxInterface {
    placeholder: string,
    value: string,
    onChange: (e: ChangeEvent<HTMLInputElement>) => void,
    type: string
}

interface AuthParams {
    signMode: "signup" | "signin"
}

const InputBox = ({
    placeholder,
    value,
    onChange,
    type
}: InputBoxInterface) => {
    return (
        <div>
            <input
                type={type}
                className="w-72 border border-black rounded text-[0.75rem] font-medium p-2 m-1 ml-0 focus:ring-1 focus:ring-black focus:outline-none"
                placeholder={placeholder}
                value={value}
                onChange={onChange}
            />
        </div>
    )
}

export const Auth = ({ signMode }: AuthParams) => {
    const navigate = useNavigate()

    const [signInInputs, setSignInInputs] = useState<SignUpSchema>({
        email: "",
        name: "",
        password: ""
    })

    const onChangeKey = (e: ChangeEvent<HTMLInputElement>, key: string) => {
        setSignInInputs({
            ...signInInputs,
            [key]: e.target.value
        })
    }

    // dont know what it does but filters only the wanted fields :)
    const filterRequestBody = <T extends object>(body: any, allowedKeys: (keyof T)[]): Partial<T> => {
        return Object.keys(body)
            .filter(key => allowedKeys.includes(key as keyof T))
            .reduce((obj, key) => {
                obj[key as keyof T] = body[key];
                return obj
            }, {} as Partial<T>);
    }

    const submitRequest = async () => {
        const allowedKeys = signMode === "signup" ? ["name", "email", "password"] : ["email", "password"]
        const requestBody = filterRequestBody({
            name: signInInputs.name,
            email: signInInputs.email,
            password: signInInputs.password
        }, allowedKeys)

        await axios.post(`${configuration.backendURL}/api/v1/user/${signMode}`, requestBody)
            .then(res => {
                localStorage.setItem("token", `Bearer ${res.data.token}`)
                navigate('/blogs')
            })
            .catch(err => {
                console.log(err)
            })
    }

    return (
        <div className="flex flex-col gap-3">
            <div className="flex items-center justify-center flex-col">
                <div className="font-bold text-2xl">{signMode === "signup" ? "Create an account" : "Welcome back"}</div>
                <div className="gap-1 flex flex-row items-center justify-center opacity-30">
                    <div>{signMode === "signup" ? "Already have an account?" : "Don't have an account?"}</div>
                    <div className="underline cursor-pointer" onClick={() => navigate(signMode === "signup" ? "/signin" : "/signup")}>{signMode === "signup" ? "Login" : "Register"}</div>
                </div>
            </div>

            <div className="flex flex-col items-center justify-center gap-4">
                {signMode === "signup" && <div className="flex items-start justify-center flex-col">
                    <div className="text-sm font-semibold">Username</div>
                    <InputBox placeholder="Enter your username" value={signInInputs.name} onChange={(e) => onChangeKey(e, "name")} type="text" />
                </div>}
                <div className="flex items-start justify-center flex-col">
                    <div className="text-sm font-semibold">Email</div>
                    <InputBox placeholder="johndoe@example.com" value={signInInputs.email} onChange={(e) => onChangeKey(e, "email")} type="email" />
                </div>
                <div className="flex items-start justify-center flex-col">
                    <div className="text-sm font-semibold">Password</div>
                    <InputBox placeholder="" value={signInInputs.password} onChange={(e) => onChangeKey(e, "password")} type="password" />
                </div>
            </div>

            <div>
                <button
                    className="bg-black text-white font-medium text-sm p-[0.45rem] w-72 rounded"
                    onClick={submitRequest}
                >
                    {signMode === "signup" ? "Register" : "Login"}
                </button>
            </div>
        </div >
    )
}
