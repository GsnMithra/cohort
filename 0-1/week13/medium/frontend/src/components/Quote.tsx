export const Quote = () => {
    return <div className="bg-slate-100 h-screen flex justify-center flex-col">
        <div className="flex justify-center">
            <div className="max-w-lg">
                <div className="text-3xl font-light">
                    "Amazing blogs I say!"
                </div>
                <div className="max-w-md text-xl font-extralight text-left mt-4">
                    Gsn Mithra
                </div>
                <div className="max-w-md text-sm font-light text-slate-400">
                    CEO | Meta
                </div>
            </div>
        </div>

    </div>
}
