import Avatar from "../assets/avatar.png"

interface BlogDisplayParams {
    authorName: string,
    uploadedAt: string,
    title: string,
    description: string
}

export const BlogDisplay = ({ authorName, uploadedAt, title, description }: BlogDisplayParams) => {
    return (
        <div className="flex flex-col border-b-[1.5px] gap-[0.25rem] pb-10 border-gray-200">
            <div className="flex gap-2 flex-row">
                <img src={Avatar} alt="Avatar" className="h-6 w-6" />
                <div className="font-semibold text-sm">{authorName}</div>
                <div className="text-sm opacity-75 font-light">{uploadedAt}</div>
            </div>
            <div className="font-extrabold">
                {title}
            </div>
            <div className="line-clamp-2 w-[50rem] text-sm">
                {description}
            </div>
        </div>
    )
}
