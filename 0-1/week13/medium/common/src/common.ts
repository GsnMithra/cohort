import { z } from "zod";

// these are the exports for the backend for safeParse ()
export const signUpSchema = z.object({
    email: z.string().email("Invalid email"),
    name: z.string().min(1),
    password: z.string().min(6)
})

export const signInSchema = z.object({
    email: z.string().email("Invalid email"),
    password: z.string().min(6)
})

export const createBlogSchema = z.object({
    title: z.string().min(1),
    content: z.string().min(1)
})

export const updateBlogSchema = z.object({
    title: z.string().min(1),
    content: z.string().min(1),
    blogId: z.string()
})

// these are the exports for the frontend
export type SignUpSchema = z.infer<typeof signUpSchema>
export type SignInSchema = z.infer<typeof signInSchema>
export type CreateBlogSchema = z.infer<typeof createBlogSchema>
export type UpdateBlogSchema = z.infer<typeof updateBlogSchema>
