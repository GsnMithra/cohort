"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateBlogSchema = exports.createBlogSchema = exports.signInSchema = exports.signUpSchema = void 0;
const zod_1 = require("zod");
// these are the exports for the backend for safeParse ()
exports.signUpSchema = zod_1.z.object({
    email: zod_1.z.string().email("Invalid email"),
    name: zod_1.z.string().min(1),
    password: zod_1.z.string().min(6)
});
exports.signInSchema = zod_1.z.object({
    email: zod_1.z.string().email("Invalid email"),
    password: zod_1.z.string().min(6)
});
exports.createBlogSchema = zod_1.z.object({
    title: zod_1.z.string().min(1),
    content: zod_1.z.string().min(1)
});
exports.updateBlogSchema = zod_1.z.object({
    title: zod_1.z.string().min(1),
    content: zod_1.z.string().min(1),
    blogId: zod_1.z.string()
});
