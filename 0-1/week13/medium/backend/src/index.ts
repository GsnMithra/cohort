import { Hono } from 'hono'
import blog from './routes/blog'
import user from './routes/user'
import { cors } from 'hono/cors'

// do NOT use global variables in a serverless environments

const app = new Hono()

app.use(cors())
app.get('/', (c) => {
    return c.text('Hello Medium!')
})

app.route('/api/v1/user', user)
app.route('/api/v1/blog', blog)

export default app
