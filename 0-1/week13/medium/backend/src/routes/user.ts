// publish to npm by `npm publish --access public`
// have "declarations": true in the tsconfig.json, build the project and point to the js file in the config.
// additional: ignore the src folder

import { signInSchema, signUpSchema } from "@gsnmithra/medium-types";
import { getPrisma } from "../database/db"
import { sign } from "hono/jwt";
import { Hono } from "hono";

const user = new Hono<{
    Bindings: {
        DATABASE_URL: string,
        JWT_SECRET: string
    },
}>()

user.post('signup', async (c) => {
    const prisma = getPrisma(c.env.DATABASE_URL)

    const body = await c.req.json()
    if (signUpSchema.safeParse(body).error)
        return c.json({
            error: "Invalid inputs!"
        })

    const { email, name, password } = body
    const response = await prisma.user.findMany({
        where: {
            email
        }
    })

    if (response.length != 0)
        return c.json({
            error: "Email already exists"
        })

    const user = await prisma.user.create({
        data: {
            email,
            name,
            password
        }
    })

    if (!user)
        return c.json({
            error: "Something went wrong!"
        })

    const token = await sign({
        id: user.id,
        email: email,
        name: name
    }, c.env.JWT_SECRET)

    return c.json({
        message: "User created successfully!",
        token: token
    })
})

user.post('signin', async (c) => {
    const prisma = getPrisma(c.env.DATABASE_URL)

    const body = await c.req.json()
    if (signInSchema.safeParse(body).error)
        return c.json({
            error: "Invalid inputs!"
        })

    const { email, password } = body

    const response = await prisma.user.findUnique({
        where: {
            email,
        }
    })

    if (!response)
        return c.json({
            error: "Email doesn't exists"
        })

    if (response.password !== password)
        return c.json({
            error: "Password doesn't match!"
        })

    const token = await sign({
        id: response.id,
        email: email,
    }, c.env.JWT_SECRET)

    return c.json({
        message: "Logged in successfully!",
        token: token
    })
})

export default user
