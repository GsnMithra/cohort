import { Hono } from "hono";
import { verify } from "hono/jwt";
import { getPrisma } from "../database/db"

const blog = new Hono<{
    Bindings: {
        DATABASE_URL: string,
        JWT_SECRET: string
    },
    Variables: {
        userId: string
    }
}>()

blog.use(async (c, next) => {
    const authorization = c.req.header("Authorization")
    if (!authorization)
        return c.json({
            error: "Authorization token not provided!"
        })

    const token = authorization.split(" ")[1]

    try {
        const payload = await verify(token, c.env.JWT_SECRET)
        c.set("userId", payload.id)
        await next()
    }
    catch (e) {
        return c.json({
            error: "Token verification failed!"
        })
    }

})

blog.post('/', async (c) => {
    const prisma = getPrisma(c.env.DATABASE_URL)

    const userId = c.get("userId")
    const body = await c.req.json()
    const { title, content } = body

    const blog = await prisma.post.create({
        data: {
            userId,
            title,
            content
        }
    })

    if (!blog)
        return c.json({
            error: "Something went wrong!"
        })

    return c.json({
        message: "Blog posted successfully!",
        blogId: blog.id
    })
})

blog.put('/', async (c) => {
    const prisma = getPrisma(c.env.DATABASE_URL)

    const userId = c.get("userId")
    const body = await c.req.json()
    const { title, content, blogId } = body

    const response = await prisma.post.update({
        where: {
            userId: userId,
            id: blogId
        },
        data: {
            title,
            content
        }
    })

    if (!response)
        return c.json({
            error: "Something went wrong!"
        })

    return c.json({
        message: "Blog updated successfully!",
    })
})

blog.get('/:id', async (c) => {
    const prisma = getPrisma(c.env.DATABASE_URL)

    const userId = c.get("userId")
    const blogId = c.req.param("id")

    const response = await prisma.post.findUnique({
        where: {
            userId: userId,
            id: blogId
        },
        select: {
            id: true,
            title: true,
            user: {
                select: {
                    id: true,
                    name: true,
                    bio: true
                }
            },
            uploadedAt: true,
            content: true
        }
    })

    if (!response)
        return c.json({
            error: "Something went wrong!"
        })

    return c.json(response)
})

blog.get('/', async (c) => {
    const prisma = getPrisma(c.env.DATABASE_URL)

    const response = await prisma.post.findMany({
        select: {
            id: true,
            title: true,
            user: {
                select: {
                    id: true,
                    name: true,
                    bio: true
                }
            },
            uploadedAt: true,
            content: true
        }
    })

    if (!response)
        return c.json({
            error: "Something went wrong!"
        })

    return c.json(response)
})

export default blog 
