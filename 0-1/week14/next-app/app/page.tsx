"use client"

import { useRouter } from "next/navigation";

export default async function Home() {
    const router = useRouter()

    return (
        <main className="flex items-center flex-col justify-center h-screen w-screen">
            <div>Welcome</div>
            <button onClick={() => router.push('/users')}>Users</button>
        </main>
    );
}
