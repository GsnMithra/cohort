import { NextRequest, NextResponse } from "next/server";
import prisma from "@/db";

export async function GET() {
    const users = await prisma.user.findMany({})
    return NextResponse.json({
        users
    })
}

export async function POST(request: NextRequest) {
    const body = await request.json()
    const { username, password } = body

    const exists = await prisma.user.findFirst({
        where: {
            username
        }
    })

    if (exists)
        return NextResponse.json({
            error: "User already exists!"
        })

    const response = await prisma.user.create({
        data: {
            username,
            password
        }
    })

    if (!response.id)
        return NextResponse.json({
            error: "Something went wrong!"
        })

    return NextResponse.json({
        id: response.id,
        message: "User created successfully!"
    })
}
