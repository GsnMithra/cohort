import { NextRequest, NextResponse } from "next/server";

export async function POST(request: NextRequest) {
    const body = await request.json()
    const queryParam = request.nextUrl.searchParams.get('name')
    const headers = request.headers.get('Authorization')

    return NextResponse.json({
        body,
        queryParam,
        headers
    })
}
