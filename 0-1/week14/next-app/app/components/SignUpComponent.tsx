"use client"

import { useState } from "react"
import axios from "axios"
import { useRouter } from "next/navigation"

export const SignUpComponent = () => {
    const router = useRouter()
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    const submitHandler = async () => {
        await axios.post('http://localhost:3000/api/user', {
            username,
            password
        }).then(res => {
            console.log(res.data)
        })

        router.push('/')
    }

    return <div className="flex flex-col gap-5 items-center justify-center h-screen w-screen">
        <input
            type="text"
            placeholder="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
        />
        <input
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
        />
        <button onClick={submitHandler}>Submit</button>
    </div>
}
