import axios from "axios";
import { getAllUsers } from "../serverActions/user"

const getUsersFromDB = async () => {
    //const users = await axios.get("http://localhost:3000/api/user")
    //return users.data
    const res = await getAllUsers();
    return res
}

export default async function() {
    const data = await getUsersFromDB()

    return (
        <main className="flex items-center flex-col justify-center h-screen w-screen">
            <div>Users</div>
            <div>{JSON.stringify(data)}</div>
        </main>
    );
}
