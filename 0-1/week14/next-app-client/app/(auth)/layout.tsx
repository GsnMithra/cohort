import { ReactNode } from "react";

export default function({ children }: { children: ReactNode }) {
    return (
        <div className="flex items-center justify-center">
            <div className="absolute top-[10%] left-[50%]">
                Banner
            </div>
            {children}
        </div>
    )
}
