interface User {
    name: string,
    age: number,
    email: string,
    password: string
}

type Student = {
    name: string,
    rollNumber: string
}

// 1. Pick
// 'picks' just the attributes that are given as the second param from the first param interface or type
type UpdatedUserProps = Pick<User, 'name' | 'age' | 'email'>
type UpdatedStudentProps = Pick<Student, 'name'>


// 2. Partial
// marks all the UpdatedUserProps attributes as optional
type UpdateUserPropsOptional = Partial<UpdatedUserProps>


// 3. Readonly
// cannot assign value to the attributes which are marked as readonly
type Todo = {
    id: number,
    readonly title: string,
    readonly description: string
}

// or just make the entire object readonly
const todo: Readonly<Todo> = {
    id: 1,
    title: 'Go to gym',
    description: 'Go to gym at 5PM'
}

// cannot do this!
//todo.title = 'Gym'


// 4. Record

// how you would normally do it (ugly syntax)
//type Users = {
//    [key: number]: User
//}

// using of records (cleaner syntax)
type Users = Record<number, User>

const users: Users = {
    1: {
        name: 'Mithra',
        age: 21,
        email: 'mithragsn9@gmail.com',
        password: 'mithra'
    },
    2: {
        name: 'Harkirat',
        age: 31,
        email: 'harkirat@gmail.com',
        password: 'cohort'
    }
}

console.log(users[1])


// 5. Map 

// without generics
//const students = new Map()

// with generics
const students = new Map<number, User>()

students.set(1, {
    name: 'Mithra',
    age: 20,
    email: 'mithragsn9@gmail.com',
    password: 'mithra'
})

const someStudent = students.get(1)
console.log(someStudent)


// 6. Exclude

type ExcludedUser = Exclude<User, 'age' | 'email'>
// ExcludedUser -> {
//  name: string,
//  password: string
// }

