"use strict";
// or just make the entire object readonly
const todo = {
    id: 1,
    title: 'Go to gym',
    description: 'Go to gym at 5PM'
};
const users = {
    1: {
        name: 'Mithra',
        age: 21,
        email: 'mithragsn9@gmail.com',
        password: 'mithra'
    },
    2: {
        name: 'Harkirat',
        age: 31,
        email: 'harkirat@gmail.com',
        password: 'cohort'
    }
};
console.log(users[1]);
// 5. Map 
// without generics
//const students = new Map()
// with generics
const students = new Map();
students.set(1, {
    name: 'Mithra',
    age: 20,
    email: 'mithragsn9@gmail.com',
    password: 'mithra'
});
const someStudent = students.get(1);
console.log(someStudent);
// ExcludedUser -> {
//  name: string,
//  password: string
// }
