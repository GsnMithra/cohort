const zod = require("zod")
const express = require("express")
const mongoose = require("mongoose")
const jwt = require("jsonwebtoken")

const app = express()
app.use(express.json())

const dbURL = process.env.MONGO_URI
const jwtSecret = process.env.JWT_SECRET

mongoose.connect(dbURL)
    .then(() => {
        console.log("connected to db")
    })
    .catch(() => {
        console.log("error occured connecting to the database")
    })

const User = mongoose.model("users", {
    name: String,
    email: String,
    password: String,
})

const zodEmail = zod.string().email()
const zodString = zod.string()

function checkEmailPass(req, res, next) {
    const password = req.body.password;
    const email = req.body.email

    if (!zodString.safeParse(password).success || !zodEmail.safeParse(email).success) {
        return res.status(412).json({
            error: "invalid input fields"
        })
    }

    next()
}

function verifySignature(req, res, next) {
    const token = req.headers.authorization;
    try {
        jwt.verify(token, jwtSecret)
        next()
    } catch (e) {
        return res.status(500).json({
            error: "invalid signature"
        })
    }
}

app.get("/some-data", verifySignature, (req, res) => {
    res.json({
        message: "some sensitive information"
    })
})

app.use(checkEmailPass)

app.get("/login", async (req, res) => {
    const password = req.body.password;
    const email = req.body.email

    const existsUser = await User.findOne({ email })
    if (!existsUser) {
        return res.status(400).json({
            error: "user doesn't exist"
        })
    }

    const validUser = existsUser.password === password
    if (!validUser) {
        return res.status(401).json({
            error: "invalid credentails"
        })
    }

    res.json({
        message: "logged in successfully"
    })
})

app.post("/signup", async (req, res) => {
    const name = req.body.name;

    if (!zodString.safeParse(name).success) {
        return res.status(412).json({
            error: "invalid input fields"
        })
    }

    const password = req.body.password;
    const email = req.body.email

    const existsUser = await User.findOne({ email: email })
    if (existsUser) {
        return res.status(412).json({
            error: "user already exists"
        })
    }

    const signature = jwt.sign({
        name,
        email
    }, jwtSecret)

    const newUser = new User({
        name,
        email,
        password,
    })

    await newUser.save();

    res.json({
        message: "user created!",
        token: signature
    })
})

app.use((err, req, res, next) => {
    res.status(500).json({
        message: "something went wrong"
    })
})

app.listen(3000)
