const express = require("express")
const app = express()

const User = require("./schema")
const mongoose = require("mongoose")
const mongoURI = "mongodb+srv://gsnmithra:mitra9812345@cluster0.qaqrezd.mongodb.net/test-data"

mongoose.connect(mongoURI)
    .then(() => {
        console.log("connected to the database!");
    })
    .catch(() => {
        console.log("error occured connecting to database")
    })

app.get('/', (req, res) => {
    res.send("Hello, from server!")
})

app.delete('/delete-user', async (req, res) => {
    const name = req.query.name;

    const usersFound = await User.findOne({
        name
    })

    if (usersFound === null) {
        res.status(400).send(`no users named: ${name}`)
        return;
    }

    const session = await mongoose.startSession();

    try {
        await User.deleteOne({
            _id: usersFound._id
        })

        session.commitTransaction()
        res.status(200).send("user deleted!")
    } catch (e) {
        session.abortTransaction()
        res.status(500).send("user deletion failed!")
    } finally {
        session.endSession()
    }
})

app.post('/create-user', async (req, res) => {
    const session = await mongoose.startSession();

    if (req.query.name === undefined || req.query.age === undefined || req.query.email === undefined) {
        res.status(400).send("user details incomplete, specify (name, age, email)")
        return;
    }

    let { name, age, email } = { ...req.query, age: parseInt(req.query.age) }
    session.startTransaction()

    try {
        const newUser = new User({
            name,
            age,
            email
        })
        await newUser.save()
        session.commitTransaction()

        res.status(200).send("user created.")
    } catch (e) {
        session.abortTransaction()
        res.status(500).send("user creation failed")
    } finally {
        session.endSession()
    }
})

const PORT = 3000;
app.listen(PORT, () => {
    console.log(`running on port: ${PORT}`)
})