const express = require("express")
const app = express()

let numRequests = 0;
let requestTimes = []

function validUserPass(req, res, next) {
    const username = req.headers.username;
    const password = req.headers.password;

    if (username === undefined || username === "" || password === undefined || password === "") {
        res.status(400).json({ message: "invalid username/password" })
        return;
    }

    next();
}

app.get("/say-hello", (req, res) => {
    const startTime = new Date();

    const name = req.query.name
    numRequests += 1;

    res.json({ message: `hello, ${name}!` })

    res.on("finish", () => {
        const endTime = new Date();
        requestTimes.push(endTime - startTime);
    });
})

app.get("/get-stats", (req, res) => {
    const startTime = new Date();
    const sumArray = requestTimes.reduce((acc, cur) => acc + cur, 0);
    numRequests += 1;

    res.json({
        numRequests: numRequests,
        requestTimes: requestTimes,
        averageRequestTime: sumArray / requestTimes.length,
    })

    res.on("finish", () => {
        const endTime = new Date();
        requestTimes.push(endTime - startTime)
    })
})

function initializeTimer(req, res, next) {
    const startTime = new Date();
    req.startTime = startTime
    next()
}

app.get("/login", initializeTimer, validUserPass, function (req, res, next) {
    const username = req.headers.username;
    const password = req.headers.password;

    if (username === "mithra" && password === "mithra")
        next();
    else
        res.status(400).json({ message: "invalid credentials" })
}, (req, res) => {
    numRequests += 1;
    res.json({ message: "logged in successfully" })

    res.on("finish", () => {
        const endTime = new Date();
        requestTimes.push(endTime - req.startTime)
    })
})

const PORT = 3000;
app.listen(PORT, () =>
    console.log(`server running on port ${PORT}`)
)