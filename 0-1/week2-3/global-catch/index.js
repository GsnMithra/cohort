const express = require("express")
const app = express()
app.use(express.json())

app.get("/num-kidneys", (req, res) => {
    const kidneys = req.body.kidneys;
    const numKidneys = kidneys.length

    res.json({ numKidneys: numKidneys })
})

app.get("/say-hello", (req, res) => {
    // throw new Error()
    res.json({
        message: "hello, world!"
    })
})

// global catch
app.use(function (err, req, res, next) {
    res.json({ message: "something went wrong in the logic" })
})

const PORT = 3000;
app.listen(PORT)