const express = require("express")
const zod = require("zod")
const app = express()
app.use(express.json())

const objectValidator = zod.object({
    email: zod.string().email(),
    password: zod.string()
})

const asignmentValidator = zod.object({
    email: zod.string().email(),
    password: zod.string().min(8),
    country: zod.literal("US").or(zod.literal("IN"))
})

function validUserPass(req, res, next) {
    const userObject = objectValidator.safeParse(req.body.user)

    // ZOD does this all for you ;)
    // if (!username || username === "" || !password || password === "") {
    //     res.status(412).json({ error: "invalid username / password" })
    // } if (username !== "mithra" || password !== "mithra") {
    //     res.status(412).json({ error: "invalid credentails" })
    // }

    if (!userObject.success) {
        res.status(412).json({ error: "invalid type of user credentiaials" })
        return;
    }

    next()
}

app.get("/login", validUserPass, (req, res) => {
    res.json({ message: "logged in successfully" })
})

app.use(function (error, req, res, next) {
    res.status(500).json({ message: "something went wrong!" })
})

app.listen(3000)