const express = require("express")
const mongoose = require("mongoose")
const app = express()
app.use(express.json())

const dbURI = process.env.DBURI;
const User = mongoose.model("User", {
    username: String,
    password: String
})

mongoose.connect(dbURI)
    .then(() => console.log("Connected to the database"))

app.post("/signup", async (req, res) => {
    const session = await mongoose.startSession();

    const username = req.body.username;
    const password = req.body.password;

    await User.findOne({
        username: username
    }).then((res) => {
        if (res) {
            return res.status(500).json({
                error: "user already exists"
            })
        }
    })

    session.startTransaction()
    const newUser = new User({
        username: username,
        password: password
    })

    try {
        await newUser.save()
    } catch (e) {
        session.abortTransaction()
    }

    session.commitTransaction()
    res.json({
        status: "user created successfully"
    })
})

const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Server running on port: ${PORT}`)
})
