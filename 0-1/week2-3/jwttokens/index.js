const express = require("express")
const jwt = require("jsonwebtoken")

const jwtSecret = process.env.JWT_SECRET
const app = express()
app.use(express.json())

const USERS = [
    {
        name: "mithra",
        password: "mitra",
        age: 19,
    },
    {
        name: "user1",
        password: "pass1",
        age: 19,
    },
    {
        name: "user2",
        password: "pass2",
        age: 19,
    }
]

function userExists(username, password) {
    return USERS.find((user) => user.name === username && user.password === password)
}

app.get("/users", (req, res) => {
    const authToken = req.headers.authorization

    try {
        const decoded = jwt.verify(authToken, jwtSecret)
        const user = USERS.filter((user) => user.name === decoded.username)
        res.json({ user })
    } catch (e) {
        res.status(412).json({ error: "invalid token" })
    }
})

app.post("/signin", (req, res) => {
    const username = req.body.username
    const password = req.body.password
    if (!userExists(username, password)) {
        res.status(412).json({ error: "username does'nt exist" })
        return;
    }

    const token = jwt.sign({ username }, jwtSecret)

    res.json({
        message: "logged in successfully", token
    })
})

app.use(function (err, req, res, next) {
    res.status(500).json({ error: "something went wrong!" })
})

app.listen(3000)