const express = require("express");
const app = express();
app.use(express.json())

function validUserPass(req, res, next) {
    const username = req.headers.username;
    const password = req.headers.password;

    if (username === "mithra" && password === "mitra") {
        req.someAttribute = "something"
        next();
    } else {
        res.status(411).json({ msg: "Invalid input query params." })
    }
}

function validKidneyId(req, res, next) {
    const kidneyId = req.query.kidneyId;

    if (kidneyId == 1 || kidneyId == 2) {
        next();
    } else {
        res.status(411).json({ msg: "Invalid input query params." })
    }
}

// app.use ([middleware1, middleware2])
// then is equivalent too sending that middleware to the endpoint
app.use([validUserPass, validKidneyId])

app.post("/kidney-replace", (req, res) => {
    res.json({ msg: "replaced kidneys" })
});

app.get("/kidney-checkup", (req, res) => {
    res.json({ msg: "all good" })
});

const PORT = 3000;
app.listen(PORT, () => {
    console.log(`server running on port: ${PORT}`);
});
