import './App.css'
import React, { useEffect, useState } from "react"
import axios from "axios"
import useSWR from "swr"
import { useInRouterContext } from 'react-router-dom'

// Functional
const FunctionalComponent = () => {
    React.useEffect(() => {
        console.log("functional component mounted")
        return () => {
            console.log("functional component unmounted")
        }
    }, [])

    return (
        <>
            Functional Component
        </>
    )
}

// Class based
class ClassComponent extends React.Component {
    componentDidMount() {
        console.log("class component mounted")
    }

    componentWillUnmount() {
        console.log("class component unmounted")
    }

    render() {
        return <>
            Class Component
        </>
    }
}

const useTodos = (n) => {
    const [todos, setTodos] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        const uri = "https://sum-server.100xdevs.com/todos";
        const fetchTodos = async () => {
            const response = await axios.get(uri);
            setTodos(response.data.todos)
            setLoading(false)
        }

        fetchTodos()
        const interval = setInterval(async () => {
            fetchTodos()
        }, n * 1000)

        return () => {
            clearInterval(interval)
        }
    }, [n])

    return { todos, loading }
}

const fetcher = async (url) => {
    const response = await axios.get(url)
    return response.data.todos;
}

const useIsOnline = () => {
    // window.navigator.onLine gets the current status of network
    const [isOnline, setIsOnline] = useState(window.navigator.onLine)

    useEffect(() => {
        window.addEventListener('online', () => {
            setIsOnline(true)
        })

        window.addEventListener('offline', () => {
            setIsOnline(false)
        })
    }, [])

    return isOnline
}

const useMouseMovements = () => {
    const [mousePosition, setMousePosition] = useState({
        x: 0,
        y: 0
    })

    useEffect(() => {
        window.addEventListener('mousemove', (e) => {
            const { clientX, clientY } = e;
            setMousePosition({ x: clientX, y: clientY })
        })

        return () => {
            window.removeEventListener('mousemove')
        }
    }, [])

    return mousePosition
}

const useInterval = (callback, interval) => {
    useEffect(() => {
        const int = setInterval(callback, interval);
        return () => {
            clearInterval(int)
        }
    }, []);

}

const useDebounce = (inputValue, debounceInterval) => {

    useEffect(() => {
        let timer = setTimeout(() => {
            // fetch data
            console.log(`request went out with ${inputValue}`)
        }, debounceInterval)

        return () => {
            clearTimeout(timer)
        }
    }, [inputValue])
}

const App = () => {
    // const [mounted, setMounted] = useState(true)
    // const [todos, setTodos] = useState([])

    // the good way to abstract away logic, use custom hooks
    // const { todos, loading } = useTodos(5)

    // the library way for data fetching
    const { data, error, isLoading } = useSWR("https://sum-server.100xdevs.com/todos", fetcher, {
        refreshInterval: 1000
    })
    const [count, setCount] = useState(0)
    const isOnline = useIsOnline()
    const { x, y } = useMouseMovements()
    const [inputValue, setInputValue] = useState("")

    useDebounce(inputValue, 1000)

    useInterval(() => {
        setCount(c => c + 1)
    }, 1000)

    // data fetching the bad way 
    // useEffect(() => {
    //     const uri = "https://sum-server.100xdevs.com/todos";
    //     (async () => {
    //         const response = await axios.get(uri);
    //         setTodos(response.data.todos)
    //         console.log(response.data.todos)
    //     })()
    // }, [])


    // useEffect(() => {
    //     setInterval(() => {
    //         setMounted((m) => !m)
    //     }, 3000)        
    // }, [])

    if (error)
        return <div>Error fetching the data</div>
    return (
        <div style={{ gap: "10px", display: "flex", alignItems: "center", justifyContent: "center", flexDirection: "column" }}>
            {isOnline ? <div>You are online</div> : <div>You are offline</div>}
            <div>X: {x} Y: {y}</div>
            <div>{count}</div>
            <div>
                <input type='text' value={inputValue} onChange={(e) => setInputValue(e.target.value)}></input>
            </div>
            {
                isLoading ? "loading..." : data.map((todo, idx) => (
                    <div key={idx}>
                        <div>Title: {todo.title}</div>
                        <div>Description: {todo.description}</div>
                    </div>
                ))
            }
        </div>
    )
}

export default App
