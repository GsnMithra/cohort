"use strict";
const greetHello = (firstName) => {
    return `Hello, ${firstName}`;
};
const sumNumbers = (first, second) => {
    return first + second;
};
const isLegalAge = (age) => {
    return age >= 18;
};
const runFunction = (callback, timeout) => {
    setTimeout(callback, timeout * 1000);
};
const doSomething = () => {
    console.log("hello");
};
console.log(greetHello("Mithra"));
console.log(sumNumbers(1, 2));
console.log(isLegalAge(17));
console.log(doSomething());
runFunction(() => {
    console.log("hello typescript");
}, 1);
