function getFirstElement<T>(array: T[]): T {
    return array[0]
}

const getLastElement = <T>(array: T[]): T => {
    return array[array.length - 1]
}

const getFirstLastElements = <T, E>(first: T[], second: E[]) => {
    console.log(getFirstElement(first), getLastElement(second))
}

const first = [10, 20, 30, 40, 50]
const second = ["hello", "world"]

// call <type> () or typescript will infer the type call ()
console.log(getFirstElement<number>(first))
console.log(getLastElement<number>(first))
getFirstLastElements<number, string>(first, second)
