"use strict";
function getFirstElement(array) {
    return array[0];
}
const getLastElement = (array) => {
    return array[array.length - 1];
};
const getFirstLastElements = (first, second) => {
    console.log(getFirstElement(first), getLastElement(second));
};
const first = [10, 20, 30, 40, 50];
const second = ["hello", "world"];
console.log(getFirstElement(first));
console.log(getLastElement(first));
getFirstLastElements(first, second);
