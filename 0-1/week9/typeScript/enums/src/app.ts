enum Direction {
    Up,
    Down,
    Left,
    Right
}

const moveDirection = (direction: Direction) => {
    console.log(`Moved to ${direction}`)
}

moveDirection(Direction.Down)

