"use strict";
var Direction;
(function (Direction) {
    Direction[Direction["Up"] = 0] = "Up";
    Direction[Direction["Down"] = 1] = "Down";
    Direction[Direction["Left"] = 2] = "Left";
    Direction[Direction["Right"] = 3] = "Right";
})(Direction || (Direction = {}));
const moveDirection = (direction) => {
    console.log(`Moved to ${direction}`);
};
moveDirection(Direction.Down);
function getFirstElement(array) {
    return array[0];
}
const getLastElement = (array) => {
    return array[array.length - 1];
};
const getFirstLastElements = (first, second) => {
    console.log(getFirstElement(first), getLastElement(second));
};
const first = [10, 20, 30, 40, 50];
const second = ["hello", "world"];
console.log(getFirstElement(first));
console.log(getLastElement(first));
getFirstLastElements(first, second);
