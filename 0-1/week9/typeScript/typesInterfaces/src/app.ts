interface User {
    firstName: string,
    lastName: string,
    email?: string,
    age: number
}

const isLegal = (user: User): boolean => {
    return user.age >= 18
}

const user: User = {
    firstName: "Gsn",
    lastName: "Mithra",
    age: 20
}

interface Person {
    name: string,
    age: number,
    greet: (prefix?: string) => void
}

class Student implements Person {
    name: string;
    age: number;
    constructor(name: string, age: number) {
        this.name = name;
        this.age = age;
    }

    greet(prefix?: string) {
        if (prefix === undefined)
            console.log(`Hello, ${this.name}`)
        else
            console.log(`${prefix} ${this.name}`)
    }
}

console.log(isLegal(user))
const student = new Student("Mithra", 20);
student.greet("Hello there")

type GreetArg = string | number;

// type A or type B
// type A | B

const greetUser = (arg: GreetArg) => {
    console.log(`Hello, ${arg}`)
}

// type A and also type B
// type A & B

type Member = {
    name: string,
    rollNumber: number
}

type Manager = {
    name: string,
    managerId: number
}

type Admin = Member & Manager

// Array of Admins
const adminArray: Admin[] = []
