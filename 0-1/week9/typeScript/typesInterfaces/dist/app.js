"use strict";
const isLegal = (user) => {
    return user.age >= 18;
};
const user = {
    firstName: "Gsn",
    lastName: "Mithra",
    age: 20
};
class Student {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
    greet(prefix) {
        if (prefix === undefined)
            console.log(`Hello, ${this.name}`);
        else
            console.log(`${prefix} ${this.name}`);
    }
}
console.log(isLegal(user));
const student = new Student("Mithra", 20);
student.greet("Hello there");
