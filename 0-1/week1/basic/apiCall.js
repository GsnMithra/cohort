const url = "https://dog.ceo/api/breeds/list/all";

async function getData() {
  const response = await fetch(url);
  return response.json();
}

const mainContent = async () => {
  // if await is used before getData (), then it stops at the data part and then proceeds
  // else we would see "Hello" first and then the data from getData ()

  getData().then((data) => {
    console.log(data);
  });

  console.log("Hello");
};

mainContent();
