// callback hell

// setTimeout(() => {
//   console.log("1");
//   setTimeout(() => {
//     console.log("2");
//     setTimeout(() => {
//       console.log("3");
//     }, 1000);
//   }, 1000);
// }, 1000);

const mithraSetTimeout = (duration) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, duration);
  });
};

mithraSetTimeout(1000)
  .then(() => {
    console.log("1");
    return mithraSetTimeout(2000);
  })
  .then(() => {
    console.log("2");
    return mithraSetTimeout(3000);
  })
  .then(() => {
    console.log("3");
  })
  .then(() => {
    console.log("Mithra");
  });
