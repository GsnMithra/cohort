const fs = require("fs");

// ugly code (without promises)
const getDataFromFile = (fileName, callback) => {
  fs.readFile(fileName, "utf-8", (_, data) => {
    callback(data);
  });
};

const showDataFromFile = (data) => {
  console.log(data);
};

getDataFromFile("content.txt", showDataFromFile);

// robust code (using promises)
const fileName = "content.txt";
const dataPromise = new Promise((resolve) => {
  fs.readFile(fileName, "utf-8", (_, data) => {
    resolve(data);
  });
});

dataPromise.then((data) => {
  console.log(data);
});

console.log("start");
const testControl = () => {
  console.log("inside function call");
  return new Promise((res) => {
    console.log("inside promise");
    setTimeout(() => {
      console.log("inside timeout");
      res("heyyy sexy!");
    }, 5000);
    console.log("line after timeout");
  });
};

console.log("middle");

testControl().then((data) => {
  console.log("reached then");
  console.log(data);
});

// start
// middle
// inside function call
// inside promise
// line after timeout
// (after 5 seconds) inside timeout
// reached then
// heyyy sexy!
