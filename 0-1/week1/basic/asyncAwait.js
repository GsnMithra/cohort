const fs = require("fs");

const getDataFromFile = (fileName, callback) => {
  fs.readFile(fileName, "utf-8", (_, data) => {
    callback(data);
  });
};

const showData = () => {
  const fileName = "content.txt";
  getDataFromFile(fileName);
};

// const getDataFromFile = (fileName) => {
//   return new Promise((resolve) => {
//     fs.readFile(fileName, "utf-8", (_, data) => {
//       resolve(data);
//     });
//   });
// };

// const showData = async () => {
//   const fileName = "content.txt";
//   const data = await getDataFromFile(fileName);
//   console.log(data);
// };

// showData();
