const firstPromise = (a, b) => {
  return new Promise((resolve, reject) => {
    if (a + b > 100) reject(new Error("Cannot give greater values than 100"));
    else resolve(a + b);
  });
};

const secondPromise = (a, b) => {
  return new Promise((resolve, reject) => {
    if (a * b > 100) reject(new Error("Cannot give greater values than 100"));
    else resolve(a * b);
  });
};

firstPromise(10, 100)
  .then((data) => {
    return secondPromise(data, 1);
  })
  .then((data) => {
    console.log(data);
  })
  .catch((e) => console.log(e));
