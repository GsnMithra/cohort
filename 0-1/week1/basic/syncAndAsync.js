const fs = require("fs");

function getDummyData() {
  console.log("Hello, Dummy!");
}

function getDataFromFile(fileName) {
  fs.readFile(fileName, "utf-8", (_, data) => {
    console.log(data);
  });
}

setTimeout(getDummyData, 1000);

getDataFromFile("content.txt");
