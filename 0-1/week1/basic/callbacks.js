const sum = (a, b) => {
  return a + b;
};

const sub = (a, b) => {
  return a - b;
};

const subOrSum = (a, b, callback) => {
  return callback(a, b);
};

console.log(subOrSum(10, 5, sum));
