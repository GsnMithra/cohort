const fs = require("fs");
const express = require("express");
const app = express();

app.get("/:name", (req, res) => {
  const name = req.params.name;
  res.send(name);
});

app.get("/say-hello/:to", (req, res) => {
  const name = req.params.to;
  let nameTo = name.charAt(0).toUpperCase() + name.substring(1);
  res.send(`Hello, ${nameTo}!`);
});

app.get("/show-file/:filename", (req, res) => {
  const fileName = req.params.filename;
  fs.readFile(fileName, "utf-8", (_, data) => {
    res.send(data);
  });
});

const PORT = 3000;
app.listen(PORT, () => {
  console.log(`App running on PORT:${PORT}`);
});
