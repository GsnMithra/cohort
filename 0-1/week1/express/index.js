const express = require("express");
const app = express();

class Kidney {
  constructor(name) {
    this.name = name;
    this.health = 100;
  }

  getName() {
    return this.name;
  }

  depleteHealth() {
    if (this.health > 0) this.health -= 1;
  }

  replenishHealth() {
    this.health = 100;
  }

  getHealth() {
    return this.health;
  }
}

let kidneys = new Array();
setInterval(() => {
  for (let i = 0; i < kidneys.length; i += 1) kidneys[i].depleteHealth();
}, 15000);

app.delete("/", (req, res) => {
  const idxKidney = req.query.idx - 1;
  kidneys.splice(idxKidney, 1);
  res.send(`Kidney ${idxKidney + 1} removed!`);
});

app.put("/", (req, res) => {
  const idxKidney = req.query.idx - 1;
  kidneys[idxKidney].replenishHealth();
  res.send(`Kidney ${idxKidney + 1} health replenished!`);
});

app.get("/", (_, res) => {
  let healthKidneys = new Array();
  for (let i = 0; i < kidneys.length; i += 1)
    healthKidneys.push([kidneys[i].getName(), kidneys[i].getHealth()]);

  const numKidneyHealth = {
    numKidneys: kidneys.length,
    kidneyHealth: healthKidneys,
  };

  res.json(numKidneyHealth);
});

app.post("/", (req, res) => {
  const kidneyName = req.query.name;
  kidneys.push(new Kidney(kidneyName));
  res.send("New kidney added!");
});

app.listen(3000);
