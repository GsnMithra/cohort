import { PrismaClient } from "@prisma/client"

const client = new PrismaClient()

const insertUser = async (username: string, email: string, password: string) => {
    await client.user.create({
        data: {
            username,
            email,
            password,
        }
    }).then(res => {
        console.log(res)
    }).catch(err => {
        console.log(err)
    })
}

const insertTodoForUserId = async (userId: number, task: string, description: string) => {
    await client.todo.create({
        data: {
            userId,
            task,
            description
        }
    }).then(res => console.log(res))
        .catch(err => console.log(err))
}

const getAllTodosWithUserData = async (userId: number) => {
    await client.todo.findMany({
        where: {
            userId
        },
        select: {
            id: true,
            userId: true,
            task: true,
            description: true,
            user: true
        }
    }).then(res => console.log(res))
        .catch(err => console.log(err))
}

const getAllUsers = async () => {
    await client.user.findMany()
        .then(data => console.log(data))
        .catch(err => console.log(err))
}

// insertUser("gsnmithra", "mithra@gmail.com", "password")
// insertUser("mithra", "mithra1@gmail.com", "password")
// getAllUsers()
// insertTodoForUserId(1, "Go to gym", "Go to gym at 5'o clock")
getAllTodosWithUserData(1)
