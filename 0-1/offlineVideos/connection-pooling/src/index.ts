import { PrismaClient } from "@prisma/client/edge"
import { withAccelerate } from "@prisma/extension-accelerate";

export interface Env {
	DATABASE_URL: string
}

export default {
	async fetch(request, env, ctx): Promise<Response> {
		const prisma = new PrismaClient({
			datasourceUrl: env.DATABASE_URL
		}).$extends(withAccelerate())

		const response = await prisma.user.create({
			data: {
				name: "Mithra",
				username: "mithra",
				password: "mithra"
			}
		})

		return new Response(`Request: ${JSON.stringify(response)}`)
	},
} satisfies ExportedHandler<Env>;
